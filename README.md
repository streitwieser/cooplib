# COOPLib

An object-oriented C library for data structures and algorithms.

A prefix is used for all methods to avoid name collisions with other libraries.  
Prefix for all methods (Class, ClassDataType and ClassContainer) is "**coop_**"
However, each class (CString, CPair,...) also has its own prefix so that you don't have to keep choosing between prefixes when programming.

# Class structure
```
Class |-> CDataType     --> CString  
      |                 |-> (CBitfield)  
      |                 |-> (CMatrix)
      |                 |-> (CVector)  
      |  
      |-> CContainer    --> (CPair)  
                        |-> (CSet)  
                        |-> (CList)  
                        |-> (CStack)  
                        |-> (CMap)  
                        |-> (CTree)  
                        |-> (CHeap)  
                        |-> (CGraph)  
```
# Methods of Class
The base class contains the following methods.
* constructor
* destructor
* coop_sizeOf
* coop_size
* coop_assign
* coop_clear
* coop_copy
* coop_compare
* coop_toString
* coop_cout

# Methods of all data type class (Class)

* coop_append

# Methods of all container class (Class)

* coop_insert
* coop_erase
* coop_get
* coop_count
* coop_find

# Methods for CString (Data type class)

* coop_substring
* coop_find
* coop_split

# Methods for CPair (Class)
Use Class, because there is no insert, erase, ...

* coop_first
* coop_second
* coop_empty

# Method for CSet (Container)
No additional methods

# Methods for CList (Container)

* coop_sort
* coop_push_front
* coop_push_back                                                                                                                                                                                         
* coop_pop_front
* coop_pop_back