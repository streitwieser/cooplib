#!/bin/bash
# Create include file macro_num_args.h with 100 arguments

FILE_NAME="macro_num_args.h"
MAX_ARGS=100

NARGS="#define NARGS(...) __NARGS(0, ## __VA_ARGS__"
__NARGS__="#define __NARGS("

for((i=0; i<=${MAX_ARGS}; ++i)); do
  NARGS=$(echo "${NARGS},$((${MAX_ARGS}-$i))")
  __NARGS__=$(echo "${__NARGS__}__$i,")
done

NARGS=$(echo "${NARGS})")
__NARGS__=$(echo "${__NARGS__}N,...) N")


cat << EOF > ${FILE_NAME}
// Use these macros to get number of variable argument list (0 to 10 arguments are possible).
// Afterwards, call new_ after type with number of variable arguments and the variable argument list
// @see https://stackoverflow.com/questions/2124339/c-preprocessor-va-args-number-of-arguments and/ot
// @see https://stackoverflow.com/questions/26682812/argument-counting-macro-with-zero-arguments-for-visualstudio/26685339#26685339

#ifndef NUM_ARGS_H_
#define NUM_ARGS_H_

${NARGS}
${__NARGS__}

#endif /* NUM_ARGS_H_ */

EOF
