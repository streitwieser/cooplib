#!/bin/bash
# Creates a skeleton for a new given class

if [ $# -ne 1 ]; then
  echo "Usage: $(basename $0) <new class name>"
  echo "for exampe: "$(basename $0)" CList"
  exit -1
else
  DATE=$(date +%Y)
  NEW_CLASS=$1
  NEW_CLASS_UPPER=${NEW_CLASS^^}
  FIRST_TWO_LETTERS=$(echo ${NEW_CLASS} | cut -c-2)
  FOLDER=$(echo ${NEW_CLASS^^[$FIRST_TWO_LETTERS]})
  FILE_NAME=$(echo "${NEW_CLASS}" | tr '[:upper:]' '[:lower:]')
  SOURCE_FILE="${FILE_NAME}.c"
  INCLUDE_FILE="${FILE_NAME}.h"
  UNIT_FILE="unit_test_${FILE_NAME}.c"
  echo "Add new folder ${FOLDER}, source file ${SOURCE_FILE}, include file ${INCLUDE_FILE} and unit test file UNIT/${UNIT_FILE} for new class ${NEW_CLASS}."
  
  mkdir ${FOLDER}

  cat <<- EOFC > ${FOLDER}/${SOURCE_FILE}
/*
 * Copyright (c) ${DATE} Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 *
 * Many objects will share the same type descriptor, i.e., they need the same amount of memory and the same methods can be applied to them.
 * We call all objects with the same type descriptor a class; a single object is called an instance of the class.
 * So far a class, an abstract data type, and a set of possible values together with operations, i.e., a data type, are pretty much the same.
 * An object is an instance of a class, i.e., it has a state represented by the memory allocated by new() and the state is manipulated with the methods of its class.
 * Conventionally speaking, an object is a value of a particular data type.
 */

#include <assert.h>

#include <string.h>		// strlen(), strcpy()
#include <stdlib.h>		// malloc()
#include <stdarg.h>		// va_arg()

#include "../Class/new.h"
#include "../Class/class.h"		// for class typedef

#include "${INCLUDE_FILE}"

/* 
  TODO: Add source code here 
*/

EOFC

  cat <<- EOFI > ${FOLDER}/${INCLUDE_FILE}
/*
 * Copyright (c) ${DATE} Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 *
 * Many objects will share the same type descriptor, i.e., they need the same amount of memory and the same methods can be applied to them.
 * We call all objects with the same type descriptor a class; a single object is called an instance of the class.
 * So far a class, an abstract data type, and a set of possible values together with operations, i.e., a data type, are pretty much the same.
 * An object is an instance of a class, i.e., it has a state represented by the memory allocated by new() and the state is manipulated with the methods of its class.
 * Conventionally speaking, an object is a value of a particular data type.
 */

#ifndef ${NEW_CLASS_UPPER}_H_
#define ${NEW_CLASS_UPPER}_H_

#include "../Class/class.h"
#include "../Class/macro_num_args.h"

/* 
  TODO: Add source code here 
*/

#endif // ${NEW_CLASS_UPPER}_H_
EOFI

  cat <<- EOFU > "Unit/${UNIT_FILE}"
/*
 * Copyright (c) ${DATE} Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 *
 * Many objects will share the same type descriptor, i.e., they need the same amount of memory and the same methods can be applied to them.
 * We call all objects with the same type descriptor a class; a single object is called an instance of the class.
 * So far a class, an abstract data type, and a set of possible values together with operations, i.e., a data type, are pretty much the same.
 * An object is an instance of a class, i.e., it has a state represented by the memory allocated by new() and the state is manipulated with the methods of its class.
 * Conventionally speaking, an object is a value of a particular data type.
 */

#include <stdlib.h>   // free
#include <string.h>

#include "minunit.h"
#include "../${FOLDER}/${INCLUDE_FILE}" 

extern int tests_run;

/* 
  TODO: Add unit test here 
*/

// collect all unit tests of ${FOLDER}
static char * ${FILE_NAME}_all_tests() {
  mu_run_init();  /* must be first, clears the test run counter */
  return 0;
}

/*
 * Run all unit tests for CList class
 */
int ut_${FILE_NAME} (void)
{  
    printf("Run unit tests for  ${FOLDER}\n");
    char *result = ${FILE_NAME}_all_tests();
    if (result != 0) {
      printf("%s\n", result);
    }
    else {
      printf("\tALL TESTS PASSED\n");
    }
    printf("\tTests run: %d\n", tests_run);
    
    return result != 0;
}   
EOFU
 
fi