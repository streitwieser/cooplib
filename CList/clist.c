/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 */

#include <assert.h>

#include <string.h> // strlen(), strcpy()
#include <stdlib.h> // malloc()

#include "../Class/new.h"

#include "clist.h"

#define IS_VALID_OBJECT_TYPE(type, obj) (type == (void *)((struct Class *)obj)->size)

// pre declaration of all methods

// Class methods
static void *List_ctor(void *self, unsigned int numargs, va_list *app);
static void *List_dtor(void *self);
static size_t List_size(const void *self);
int List_assign(void *self, unsigned int numargs, va_list *app);
void List_clear(void *self);
static void *List_copy(const void *self);
static int List_compare(const void *self, const void *b);
static char *List_to_string(const void *self);
static void List_cout(const void *self);

// Container methods
int List_insert(void *self, unsigned int numargs, va_list *app);
int List_erase(void *self, unsigned int pos);
void *List_get(const void *self, size_t pos);
unsigned int List_count(const void *self, class_storeage_u object);
void *List_find(void *self, class_storeage_u object);

// CList methods
clist_error_t List_push_back(void *self, class_storeage_u object);
clist_error_t List_push_front(void *self, class_storeage_u object);
void *List_pop_back(void *self);
void *List_pop_front(void *self);
void List_sort(void *self);

struct ClassList
{
  const struct ClassContainer class; /* Base class methods must be first */

  /* Additional methods */
  clist_error_t (*push_back)(void *self, class_storeage_u object);
  clist_error_t (*push_front)(void *self, class_storeage_u object);
  void *(*pop_back)(void *self);
  void *(*pop_front)(void *self);
  void (*sort)(void *self);
};

/*
 * Define new type CList class
 * Size is the size of this data structure, size of the class List.
 * The next function pointers assign the construct, destructor, ...
 */
static const struct ClassList CListClass = {
    /* Attributes and methods of Class */
    {
        .class.sizeOf = sizeof(struct CList),

        .class.ctor = List_ctor,
        .class.dtor = List_dtor,
        .class.size = List_size,
        .class.assign = List_assign,
        .class.clear = List_clear,
        .class.copy = List_copy,
        .class.compare = List_compare,
        .class.to_string = List_to_string,
        .class.cout = List_cout,

        .insert = List_insert,
        .erase = List_erase,
        .count = List_count,
        .find = List_find,
        .get = List_get,
    },

    /* Methods of class CList */
    .push_back = List_push_back,
    .push_front = List_push_front,
    .pop_back = List_pop_back,
    .pop_front = List_pop_front,
    .sort = List_sort,

};

/** This is the memory of the CLASS data (not of the object data) */
const void *CList = &CListClass;

// Constructor of the List class
//
// The constructor retrieves the text passed to new() and stores a dynamic copy in the struct List which was allocated by new().
// In the constructor we only need to initialize object data because new() has already set up .class.
// app: arguments, here the data type of the set and a method to compare inserted data
static void *List_ctor(void *self, unsigned int numargs, va_list *app)
{
  struct CList *clist = self;
  assert(numargs == 2);

  clist->type = va_arg(*app, class_type_e);                           // type of list
  clist->compare = va_arg(*app, int (*)(void const *, void const *)); // compare method of object

  clist->head = NULL;
  clist->elements = 0;

  return clist;
}

// Destructor of the List class.
// The destructor frees the dynamic memory controlled by the List.
// Since delete() can only call the destructor if self is not null, we do not need to check things
static void *List_dtor(void *self)
{
  struct CList *clist = self;

  List_clear(clist);

  clist->head = NULL;
  clist->elements = 0;

  return clist;
}

// Returns the number of objects stored into list
static size_t List_size(const void *self)
{
  const struct CList *clist = self;
  return clist->elements;
}

// Assign new contents to list
int List_assign(void *self, unsigned int numargs, va_list *app)
{
  struct CList *clist = self;
  class_storeage_u store = {0};

  assert(clist != NULL);
  assert(numargs > 0);

  // first clear current list
  List_clear(self);

  for (unsigned int i = 0; i < numargs; i++)
  {
    switch (clist->type)
    {
      case COOP_CHAR:    store.c = va_arg(*app, int);  break;
      case COOP_INT:     store.i = va_arg(*app, int);  break;
      case COOP_LONG:    store.l = va_arg(*app, long); break;
      case COOP_DOUBLE:  store.d = va_arg(*app, double); break;
      case COOP_FLOAT:   store.f = (float)va_arg(*app, double); break;
      case COOP_OBJECT:  store.v = va_arg(*app, void *); break;
      case COOP_UNKOWN:
      default:
        assert(0);
        break;
    }

    clist_insert(self, store, clist->elements);
  }

  return NO_ERROR;
}

// Clear list
void List_clear(void *self)
{
  struct CList *clist = self;
  assert(clist != NULL);

  clist_node_t *p = clist->head;
  void *pfree = NULL;

  while (p != NULL)
  {
    if (clist->type == COOP_OBJECT)
    {
      delete (p->object.v);
    }

    pfree = p;
    p = (clist_node_t *)p->next;

    free(pfree);
  }

  clist->head = NULL;
  clist->elements = 0;
}

// Copy set object.
// List_copy() makes a copy of a List. Later both, the original and the copy,
// will be passed to delete() so we must make a new dynamic copy of the object
// This can easily be done by calling new() and insert().
static void *List_copy(const void *self)
{
  struct CList const *clist = self;
  clist_node_t *p = clist->head;
  unsigned int pos = 0;

  void *new_list = new (CList, clist->type, clist->compare);
  assert(new_list != NULL);

  while (p != NULL)
  {
    clist_insert(new_list, p->object, pos++);
    p = (clist_node_t *)p->next;
  }

  return new_list;
}

// Compares the both CList objects
static int List_compare(const void *self, const void *b)
{
  struct CList const *clist = self;
  struct CList const *clist2 = b;
  assert( clist != NULL);
  assert( clist2 != NULL);

  clist_node_t *first = clist->head;
  clist_node_t *second = clist2->head;

  int cmp = 0;

  while (first != NULL && second != NULL)
  {
    if (clist->type == COOP_OBJECT)
      cmp = clist->compare(first->object.v, second->object.v);
    else
      cmp = clist->compare(&first->object, &second->object);

    if (cmp != 0)
      break;

    first = first->next;
    second = second->next;
  }

  if (first != NULL && second == NULL)
    return 1;

  if (first == NULL && second != NULL)
    return -1;

  return cmp;
}

// Get object as char[]
static char *List_to_string(const void *self)
{
  char buffer[100];
  struct CList const *clist = self;
  clist_node_t *p = clist->head;
  size_t size = 0, len = 0;
  char *pChar = NULL;
  char *pText = NULL;

  if (p == NULL) // empty list
  {
    len = 3;
    pText = calloc(1, len);
    pText = strcat(pText, "[]");
    pText[len - 1] = '\0';
    return pText;
  }

  while (p != NULL)
  {
    if (clist->type == COOP_OBJECT)
    {
      pChar = clist_toString(p->object.v);
      len = strlen(pChar) + 1;
    }
    else
    {
      switch (clist->type)
      {
        case COOP_CHAR:    sprintf(buffer, "%c", p->object.c); break;
        case COOP_INT:     sprintf(buffer, "%d", p->object.i); break;
        case COOP_LONG:    sprintf(buffer, "%ld", p->object.l);  break;
        case COOP_FLOAT:   sprintf(buffer, "%.3f", p->object.f); break;
        case COOP_DOUBLE:  sprintf(buffer, "%.6f", p->object.d); break;
        case COOP_OBJECT:
        default:
          assert(0);
          break;
      }

      pChar = buffer;
      len = strlen(pChar) + 1;
    }

    if (size == 0)
    {
      len++;
      pText = calloc(1, len);
      size += len;
      pText = strcat(pText, "[");
    }
    else
    {
      size += len + 1;
      pText = realloc(pText, size);
      pText = strcat(pText, ",");
    }

    pText = strcat(pText, pChar);
    if (clist->type == COOP_OBJECT)
      free(pChar);

    p = (clist_node_t *)p->next;
  }

  pText = realloc(pText, size + 2);
  pText = strcat(pText, "]");

  return pText;
}

// Output text to console
static void List_cout(const void *self)
{
  char *p = List_to_string(self);
  printf("%s\n", p);
  free(p);
}

// Insert new object
int List_insert(void *self, unsigned int numargs, va_list *app)
{
  struct CList *clist = self;
  class_storeage_u object = {0};
  class_storeage_u store = {0};
  clist_node_t **p = &clist->head;
  unsigned int pos = 0;

  assert(clist != NULL);
  assert(numargs == 2);

  object = va_arg(*app, class_storeage_u);
  pos = va_arg(*app, unsigned int);

  // check list size and positions
  if (pos > clist->elements)
    return CLIST_INVALID_POSITION;

  switch (clist->type)
  {
    case COOP_OBJECT:
      store.v = clist_copy(object.v);
      break;
    default:
      memcpy(&store, &object, sizeof(class_storeage_u));
      break;
  }

  clist_node_t *new = calloc(1, sizeof(clist_node_t));
  memcpy(&new->object, &store, sizeof(class_storeage_u));
  new->next = NULL;

  while (*p != NULL && pos > 0)
  {
    p = &(*p)->next;
    pos--;
  }

  new->next = *p;
  *p = new;

  clist->elements++;

  return NO_ERROR;
}

// Erase element from list on given position
int List_erase(void *self, unsigned int pos)
{
  struct CList *clist = self;
  class_storeage_u object = {0};
  clist_node_t *p = clist->head;
  void *pfree = NULL;

  if (pos > clist->elements - 1)
    return CLIST_INVALID_POSITION;

  // search element
  if (pos == 0)
  {
    memcpy(&object, &clist->head->object, sizeof(class_storeage_u));
    pfree = clist->head;
    clist->head = clist->head->next;
  }
  else
  {
    pos--;
    while (p != NULL && pos > 0)
    {
      p = p->next;
      pos--;
    }

    object = p->next->object;
    pfree = p->next;

    // current pointer points now to next element!
    p->next = p->next->next;
  }

  if (clist->type == COOP_OBJECT)
    delete (object.v);

  free(pfree);

  clist->elements--;

  return NO_ERROR;
}

// Get object from position pos
void *List_get(const void *self, size_t pos)
{
  struct CList const *clist = self;
  class_storeage_u object = {0};
  clist_node_t *p = clist->head;

  assert(clist != NULL);

  if (pos > clist->elements - 1)
    return NULL;

  // search element
  if (pos == 0)
  {
    memcpy(&object, &clist->head->object, sizeof(class_storeage_u));
  }
  else
  {
    while (p != NULL && pos > 0)
    {
      p = p->next;
      pos--;
    }

    // give this object back
    object = p->object;
  }

  if (clist->type == COOP_OBJECT)
  {
    class_storeage_u store = p->object;
    void *p = clist_copy(store.v);
    return p;
  }
  else
  {
    // we return a void pointer to memory -> access memory.
    void *o = calloc(1, sizeof(class_storeage_u));
    memcpy(o, &p->object, sizeof(class_storeage_u));
    return o;
  }
}

// Count number of objects
unsigned int List_count(const void *self, class_storeage_u object)
{
  struct CList const *clist = self;
  clist_node_t *p = clist->head;
  unsigned int count = 0;

  assert(clist != NULL);
  assert(object.v != NULL);

  while (p != NULL)
  {
    if (clist->type == COOP_OBJECT)
    {
      if( clist->compare(object.v, p->object.v) == 0)
        count++;
    }
    else
    {
      if( clist->compare(&object, &p->object) == 0)
        count++;
    }
    p = p->next;
  }

  return count;
}

// Find given object
void *List_find(void *self, class_storeage_u object)
{
  struct CList *clist = self;
  clist_node_t *p = clist->head;
  unsigned int equal = 0;
  assert(clist != NULL);

  while (p != NULL)
  {
    if (clist->type == COOP_OBJECT)
      equal = clist->compare(p->object.v, object.v) == 0;
    else
      equal = clist->compare(&p->object, &object) == 0;

    if (equal)
      break;

    p = p->next;
  }

  if (equal)
  {
    if (clist->type == COOP_OBJECT)
      return p->object.v;
    else
      return &p->object;
  }
  else
    return NULL;
}

// Insert new object at end of list
clist_error_t List_push_back(void *self, class_storeage_u object)
{
  struct CList *clist = self;
  return clist_insert(self, object, clist->elements);
}

// Insert new object at start of list
clist_error_t List_push_front(void *self, class_storeage_u object)
{
  return clist_insert(self, object, 0);
}

// Get object from end of list and erase from list
void *List_pop_back(void *self)
{
  struct CList const *clist = self;
  void *p = List_get(self, clist->elements - 1);
  List_erase(self, clist->elements - 1);
  return p;
}

// Get object from start of list and erase from list
void *List_pop_front(void *self)
{
  void * p = List_get(self, 0);
  List_erase(self, 0);
  return p;
}

// helpfer function that swaps both element pointer and fix first pointer
int swap(void *self, clist_node_t **first)
{
  struct CList *clist = self;
  clist_node_t *second = (*first)->next;
  int lower = 0;

  if (clist->type == COOP_OBJECT)
    lower = clist->compare(second->object.v, (*first)->object.v);
  else
  {
    lower = clist->compare(&second->object, &(*first)->object);
  }

  if (lower < 0)
  {
    // link from first to third
    // link from second to first
    // second is now first
    (*first)->next = second->next;
    second->next = *first;
    *first = second;

    return 1;
  }

  return 0;
}

// Sort complete list with given compare method by construction
void List_sort(void *self)
{
  struct CList *clist = self;
  clist_node_t **p = &clist->head;
  unsigned int swapped = 0;

  assert(clist != NULL);

  if (p == NULL)
    return;

  do
  {
    swapped = 0;
    p = &clist->head;

    while ((*p)->next != NULL)
    {
      swapped |= swap(self, p);
      p = &(*p)->next;
    }
  } while (swapped);
}

//###########################################################################################
// Wrapper metods for Class and ClassContainer
//###########################################################################################

// Wrapper for Class

inline size_t clist_sizeOf(const void *self) { return coop_sizeOf(self); }
inline size_t clist_size(const void *self) { return coop_size(self); }
// clist_assign defined by macro
inline void clist_clear(void *self) { coop_clear(self); }
inline void *clist_copy(const void *self) { return coop_copy(self); }
inline int clist_compare(const void *self, const void *second) { return coop_compare(self, second); }
inline char *clist_toString(const void *self) { return coop_toString(self); }
inline void clist_cout(const void *self) { coop_cout(self); }

// Wrapper for ClassDataType

// clist_insert_ defines by macro
int clist_erase(void *self, unsigned int pos) { return coop_erase(self, pos); }
void *clist_get(const void *self, size_t pos) { return coop_get(self, pos); }
// clist_count defined by macro
// clist_find_ defined by macro

//###########################################################################################
// Access class methods over this functions -> check pointers, ...
//###########################################################################################

// Insert new object at end of list
clist_error_t clist_push_back_(void *self, class_storeage_u object)
{
  const struct ClassList *const *cp = self;
  assert(self && *cp && (*cp)->push_back);
  return (*cp)->push_back(self, object);
}

// Insert new object at start of list
clist_error_t clist_push_front_(void *self, class_storeage_u object)
{
  const struct ClassList *const *cp = self;
  assert(self && *cp && (*cp)->push_front);
  return (*cp)->push_front(self, object);
}

// Get object from end of list.
void *clist_pop_back(void *self)
{
  const struct ClassList *const *cp = self;
  assert(self && *cp && (*cp)->pop_back);
  return (*cp)->pop_back(self);
}

// Get object from start of list.
void *clist_pop_front(void *self)
{
  const struct ClassList *const *cp = self;
  assert(self && *cp && (*cp)->pop_front);
  return (*cp)->pop_front(self);
}

// Sort complete list with given compare method by construction
void clist_sort(void *self)
{
  const struct ClassList *const *cp = self;
  assert(self && *cp && (*cp)->sort);
  return (*cp)->sort(self);
}
