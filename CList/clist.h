/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_LIST_H_
#define C_LIST_H_

#include "../Class/c_container.h"
#include "../Class/macro_num_args.h"

enum
{
  CLIST_ERROR_ID = 0x000000434c697374
}; // CList in hex

// Define error type
typedef enum
{
  CLIST_NO_ERROR = NO_ERROR,
  CLIST_INVALID_OBJECT = ((CLIST_ERROR_ID & 0x00FFFFFF) << 8 | 0x80000001),
  CLIST_INVALID_POSITION = ((CLIST_ERROR_ID & 0x00FFFFFF) << 8 | 0x80000002),

} clist_error_t;

/**
 * Define node structure for list of objects
 */
typedef struct clist_node
{
  class_storeage_u object; //!< stores list elements
  struct clist_node *next; //!< pointer to next object
} clist_node_t;

/**
 * The list class.
 * It contains the class. With size, constructor, destructor, and other methods.
 * And is contains a pointer to the object data.
 */
struct CList
{
  const void *ClassContainer;                 /* base class, must be first */

  class_type_e type;                          /* store type of set */
  int (*compare)(void const *, void const *); /* compare mehtod of object */
  clist_node_t *head;                         /* linked list */
  size_t elements;                            /* number of elements */
};

/** Pointer to the list class memory */
extern const void *CList;

/** Gets the size of the object memory. It is a wrapper method for coop_sizeOf() */
extern size_t clist_sizeOf(const void *self);
/** Returns the number of objects stored into list. It is a wrapper macro for coop_size()  */
extern size_t clist_size(const void *self);
/** Assign new elements to object. It is a wrapper macro for coop_assign() */
#define clist_assign(self, ...) coop_assign_(self, NARGS(__VA_ARGS__), __VA_ARGS__)
/** Clear all elements of CList. It is a wrapper method for coop_clear() */
extern void clist_clear(void *self);
/** Copy CList object. It is a wrapper method for coop_copy() */
extern void *clist_copy(const void *self);
/** Compares two CList objects. It is a wrapper method for coop_compare() */
extern int clist_compare(const void *self, const void *second);
/** Converts the CList elements to a string. It is a wrapper method for coop_toString() */
extern char *clist_toString(const void *self);
/** Outputs CList as text to the console. It is a wrapper method for coop_cout() */
extern void clist_cout(const void *self);

// Wrapper for ClassContainer

/** This macro casts the two arguments of clist_insert */
#define CLIST_INSERT_ARGS(object, pos) (class_storeage_u)object, (unsigned int)pos

/** Insert new object. It is a wrapper macro for coop_insert() */
#define clist_insert(self, ...) coop_insert_(self, NARGS(__VA_ARGS__), CLIST_INSERT_ARGS(__VA_ARGS__) )

/** Erase element from list on given position. It is a wrapper method for coop_erase() */
int clist_erase(void *self, unsigned int pos);
/** Get element on position pos. It is a wrapper method for coop_get() */
void *clist_get(const void *self, size_t pos);
/** Count number of given element. It is a wrapper macro for coop_count() */
#define clist_count(self, object) coop_count_(self, (class_storeage_u)object)
/** Find given element. It is a wrapper macro for coop_find() */
#define clist_find(self, object) coop_find_(self, (class_storeage_u)object)

// Methods for CList only

/**
 * Insert new object at end of list
 * @param self, pointer to class data
 * @param object, pointer to object
 * @return error, 0 = no error, -1 = not unique
 */
#define clist_push_back(self, object) clist_push_back_(self, (class_storeage_u)object)
clist_error_t clist_push_back_(void *self, class_storeage_u object);

/**
 * Insert new object at start of list
 * @param self, pointer to class data
 * @param object, pointer to object
 * @return error, 0 = no error, -1 = not unique
 */
#define clist_push_front(self, object) clist_push_front_(self, (class_storeage_u)object)
clist_error_t clist_push_front_(void *self, class_storeage_u object);

/**
 * Get object from end of list.
 * Pleate note, you get a memory pointer, you must free this memory
 * @param self, pointer to class data
 * @return NULL on error, otherwise memory pointer to object
 */
void *clist_pop_back(void *self);

/**
 * Get object from start of list.
 * Pleate note, you get a memory pointer, you must free this memory
 * @param self, pointer to class data
 * @return NULL on error, otherwise memory pointer to object
 */
void *clist_pop_front(void *self);

/**
 * Sort complete list with given compare method by construction
 * @param self, pointer to class data
 */
void clist_sort(void *self);

#endif /* C_LIST_H */