/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 */

#include <assert.h>

#include <string.h> // strlen(), strcpy()
#include <stdlib.h> // malloc()

#include "../Class/new.h"

#include "cpair.h"

#define VALID_CPAIR_TYPE(x) (x >= COOP_CHAR && x <= COOP_OBJECT)

// pre declaration of all methods

// Class methods
static void *Pair_ctor(void *self, unsigned int numargs, va_list *app);
static void *Pair_dtor(void *self);
static size_t Pair_size(void const *self);
static int Pair_assign(void *self, unsigned int numargs, va_list *app);
static void Pair_clear(void *self);
static void *Pair_copy(const void *self);
static int Pair_compare(const void *self, const void *second);
static char *Pair_to_string(const void *self);
static void Pair_cout(const void *self);

// CPair methods
static class_storeage_u Pair_first(const void *self);
static class_storeage_u Pair_second(const void *self);
static unsigned int Pair_empty(void *self);

/**
 * Define new type ClassPair for CPair
 * Contains attributes and methods of Class and
 * additional methods of CPair
 */
struct ClassPair
{
  const struct Class class; /* Class methods must be first */

  /* Additional methods */
  class_storeage_u (*first)(const void *self);
  class_storeage_u (*second)(const void *self);
  unsigned int (*empty)(void *self);
};

/**
 * Define new type CPairClass
 * SizeOf is the size of this data structure, size of the class Pair.
 * The next function pointers assign the construct, destructor, ...
 */
static const struct ClassPair CPairClass = {
    /* Attributes and methods of Class */
    {
        .sizeOf = sizeof(struct CPair),

        .ctor = Pair_ctor,
        .dtor = Pair_dtor,
        .size = Pair_size,
        .assign = Pair_assign,
        .clear = Pair_clear,
        .copy = Pair_copy,
        .compare = Pair_compare,
        .to_string = Pair_to_string,
        .cout = Pair_cout,
    },

    /* Methods of class CPair */
    .first = Pair_first,
    .second = Pair_second,
    .empty = Pair_empty,
};

/** This is the memory of the CLASS data (not of the object data) */
const void *CPair = &CPairClass;

// Constructor of the Pair class
//
// The constructor retrieves the arguments passed to new() and stores a dynamic copy in the struct Pair which was allocated by new().
// In the constructor we only need to initialize object data because new() has already set up .class.
// app: type of first object, first object, fist compare function, type of second object, second object, second compare function
static void *Pair_ctor(void *self, unsigned int numargs, va_list *app)
{
  struct CPair *cpp = self;
  class_storeage_u store1 = {0};
  class_storeage_u store2 = {0};
  assert(numargs == 6);

  // first element
  cpp->first.type = va_arg(*app, int);
  assert(VALID_CPAIR_TYPE(cpp->first.type));

  store1 = va_arg(*app, class_storeage_u);
  if (cpp->first.type == COOP_OBJECT)
  {
    void *p = cpair_copy(store1.v);
    store1.v = p;
  }
  memcpy(&cpp->first.object, (void *)&store1, sizeof(class_storeage_u));

  cpp->first.compare = va_arg(*app, int (*)(void const *, void const *));
  assert(cpp->first.compare != NULL);

  // second element
  cpp->second.type = va_arg(*app, int);
  assert(VALID_CPAIR_TYPE(cpp->second.type));

  store2 = va_arg(*app, class_storeage_u);
  if (cpp->second.type == COOP_OBJECT)
  {
    void *p = cpair_copy(store2.v);
    store2.v = p;
  }
  memcpy(&cpp->second.object, (void *)&store2, sizeof(class_storeage_u));

  cpp->second.compare = va_arg(*app, int (*)(void const *, void const *));
  assert(cpp->second.compare != NULL);

  return cpp;
}

// Destructor of the Pair class.
// The destructor frees the dynamic memory controlled by the Pair.
// Since delete() can only call the destructor if self is not null, we do not need to check things
static void *Pair_dtor(void *self)
{
  struct CPair *cpp = self;

  if (cpp->first.type == COOP_OBJECT)
    delete (cpp->first.object.v);

  if (cpp->second.type == COOP_OBJECT)
    delete (cpp->second.object.v);

  return cpp;
}

// Returns the number of elements. By a pair allways 2 */
static size_t Pair_size(void const *self)
{
  struct CPair const *cpp = self;
  assert( cpp != NULL );
  return 2;
}

// Assign new elements to pair
static int Pair_assign(void *self, unsigned int numargs, va_list *app)
{
  struct CPair *cpp = self;
  class_storeage_u store1 = {0};
  class_storeage_u store2 = {0};
  class_type_e first = cpp->first.type;
  class_type_e second = cpp->second.type;

  if (numargs == 2)
  {
    Pair_clear(self);

    cpp->first.type = first;
    cpp->second.type = second;

    store1 = va_arg(*app, class_storeage_u);
    if (cpp->first.type == COOP_OBJECT)
    {
      void *p = cpair_copy(store1.v);
      store1.v = p;
    }
    memcpy(&cpp->first.object, (void *)&store1, sizeof(class_storeage_u));

    store2 = va_arg(*app, class_storeage_u);
    if (cpp->second.type == COOP_OBJECT)
    {
      void *p = cpair_copy(store2.v);
      store2.v = p;
    }
    memcpy(&cpp->second.object, (void *)&store2, sizeof(class_storeage_u));

    return CPAIR_NO_ERROR;
  }

  return CPAIR_NUMBER_ARGUMENTS;
}

// Clear both elements of pair object
static void Pair_clear(void *self)
{
  struct CPair *cpp = self;

  if (cpp->first.type == COOP_OBJECT)
    delete (cpp->first.object.v);

  if (cpp->second.type == COOP_OBJECT)
    delete (cpp->second.object.v);

  cpp->first.object.v = NULL;
  cpp->second.object.v = NULL;
  cpp->first.type = COOP_UNKOWN;
  cpp->second.type = COOP_UNKOWN;
}

// Copy set object.
// Set_copy() makes a copy of a pair. Later both, the original and the copy,
// will be passed to delete() so we must make a new dynamic copy of the object
// This can easily be done by calling new()
static void *Pair_copy(const void *self)
{
  struct CPair const *cpp = self;
  assert(cpp != NULL);

  void *new_pair = new_(CPair, 6, cpp->first.type, (class_storeage_u)cpp->first.object, cpp->first.compare, cpp->second.type, (class_storeage_u)cpp->second.object, cpp->second.compare);
  assert(new_pair != NULL);

  return new_pair;
}

// Compares the both CPair objects
static int Pair_compare(const void *self, const void *second)
{
  struct CPair const *cpp = self;
  struct CPair const *check = second;
  unsigned int cmp = 0;

  assert(cpp != NULL);
  assert(check != NULL);

  if (cpp == check)
    return 0;

  if (!check)
    return 1;

  if (cpp->first.type != check->first.type)
    return 1;

  if (cpp->second.type != check->second.type)
    return 1;

  if (cpp->first.type == COOP_OBJECT)
    cmp = cpp->first.compare(cpp->first.object.v, check->first.object.v);
  else
    cmp = cpp->first.compare(&cpp->first.object, &check->first.object);

  if (cmp == 0)
  {
    if (cpp->second.type == COOP_OBJECT)
      cmp = cpp->second.compare(cpp->second.object.v, check->second.object.v);
    else
      cmp = cpp->second.compare(&cpp->second.object, &check->second.object);
  }

  return cmp;
}

// Get object as char[]
static char *Pair_to_string(const void *self)
{
  char buffer[100];
  char *p = buffer;
  struct CPair const *cpp = self;
  char *text = 0;

  switch (cpp->first.type)
  {
  case COOP_UNKOWN:
    sprintf(buffer, "( unknown");
    break;
  case COOP_CHAR:
    sprintf(buffer, "( %c", cpp->first.object.c);
    break;
  case COOP_INT:
    sprintf(buffer, "( %d", cpp->first.object.i);
    break;
  case COOP_LONG:
    sprintf(buffer, "( %ld", cpp->first.object.l);
    break;
  case COOP_FLOAT:
    sprintf(buffer, "( %.3f", cpp->first.object.f);
    break;
  case COOP_DOUBLE:
    sprintf(buffer, "( %.6f", cpp->first.object.d);
    break;
  case COOP_OBJECT:
    p = coop_toString(cpp->first.object.v);
    sprintf(buffer, "( %s", p);
    free(p);
    break;
  }
  size_t size1 = strlen(buffer);

  switch (cpp->second.type)
  {
  case COOP_UNKOWN:
    sprintf(buffer + size1, ", unknown )");
    break;
  case COOP_CHAR:
    sprintf(buffer + size1, ", %c )", cpp->second.object.c);
    break;
  case COOP_INT:
    sprintf(buffer + size1, ", %d )", cpp->second.object.i);
    break;
  case COOP_LONG:
    sprintf(buffer + size1, ", %ld )", cpp->second.object.l);
    break;
  case COOP_FLOAT:
    sprintf(buffer + size1, ", %.3f )", cpp->second.object.f);
    break;
  case COOP_DOUBLE:
    sprintf(buffer + size1, ", %.6f )", cpp->second.object.d);
    break;
  case COOP_OBJECT:
    p = coop_toString(cpp->second.object.v);
    sprintf(buffer + size1, ", %s )", p);
    free(p);
    break;
  }

  size_t size2 = strlen(buffer);
  text = calloc(1, size2 + 1);
  text = strncat(text, buffer, size2);
  text[size2] = '\0';

  return text;
}

// Output text to console
static void Pair_cout(const void *self)
{
  char *p = Pair_to_string(self);
  printf("%s\n", p);
  free(p);
}

// Get the first element
// if not a object return value from store.
// if an object then create a new object and return pointer in store.
static class_storeage_u Pair_first(const void *self)
{
  struct CPair const *cpp = self;
  if (cpp->first.type != COOP_OBJECT)
    return cpp->first.object;
  else
  {
    class_storeage_u store = {0};
    void *p = cpair_copy(cpp->first.object.v);
    store.v = p;
    return store;
  }
}

// Get the second element
// if not a object return value from store.
// if an object then create a new object and return pointer in store.
static class_storeage_u Pair_second(const void *self)
{
  struct CPair const *cpp = self;
  if (cpp->second.type != COOP_OBJECT)
    return cpp->second.object;
  {
    class_storeage_u store = {0};
    void *p = cpair_copy(cpp->second.object.v);
    store.v = p;
    return store;
  }
}

// Check if pair is empty, cleared!
static unsigned int Pair_empty(void *self)
{
  struct CPair *cpp = self;
  return (cpp->first.type == COOP_UNKOWN) || (cpp->second.type == COOP_UNKOWN);
}

//###########################################################################################
// Wrapper metods for Class and ClassDataType
//###########################################################################################

// Wrapper for Class
inline size_t cpair_sizeOf(const void *self) { return coop_sizeOf(self); }
inline size_t cpair_size(const void *self) { return coop_size(self); }
inline void cpair_clear(void *self) { coop_clear(self); }
inline void *cpair_copy(const void *self) { return coop_copy(self); }
inline int cpair_compare(const void *self, const void *second) { return coop_compare(self, second); }
inline char *cpair_toString(const void *self) { return coop_toString(self); }
inline void cpair_cout(const void *self) { coop_cout(self); }

//###########################################################################################
// Access class methods over this functions -> check pointers, ...
//###########################################################################################

// Get the first element
class_storeage_u cpair_first(const void *self)
{
  const struct ClassPair *const *cp = self;
  assert(self && *cp && (*cp)->first);
  return (*cp)->first(self);
}

// Get the second element
class_storeage_u cpair_second(const void *self)
{
  const struct ClassPair *const *cp = self;
  assert(self && *cp && (*cp)->second);
  return (*cp)->second(self);
}

// Check if pair is empty, cleared!
unsigned int cpair_empty(void *self)
{
  const struct ClassPair *const *cp = self;
  assert(self && *cp && (*cp)->empty);
  return (*cp)->empty(self);
}