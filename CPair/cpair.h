/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 */

#ifndef C_PAIR_H_
#define C_PAIR_H_

#include "../Class/class.h"

enum { CPAIR_ERROR_ID = 0x0000004350616972 }; // CPair in hex

/**
 * Define error type for CPair class
 */
typedef enum
{
  CPAIR_NO_ERROR = NO_ERROR,
  CPAIR_NUMBER_ARGUMENTS = ((CPAIR_ERROR_ID & 0x00FFFFFF) << 8 | 0x80000001),
} cpair_error_t;

/**
 * Data structure for a element of CPair class
 */
typedef struct
{
  class_type_e type;                          //!< Type of element
  int (*compare)(void const *, void const *); //!< Compare method for element
  class_storeage_u object;                    //!< Store element
} cpair_element_t;

/**
 * The pair class.
 * Is derived from Class.
 * It contains the pointer to base class.
 * And is contains a two elements, first and second
 */
struct CPair
{
  const void *Class;      //!< Pointer to class, must be first by all classes

  cpair_element_t first;  //!< first element
  cpair_element_t second; //!< second element
};

/** Pointer to the pair class memory */
extern const void *CPair;

// Wrapper for Class

/** Gets the size of the object memory. It is a wrapper method for coop_sizeOf() */
extern size_t cpair_sizeOf(const void *self);
/** Returns the length of the string. It is a wrapper method for coop_length() */
extern size_t cpair_size(const void *self);
/** Assign new string to object. It is a wrapper macro for coop_assign() */
#define cpair_assign(self, ...) coop_assign_(self, NARGS(__VA_ARGS__), __VA_ARGS__)
/** Clear string of CString. It is a wrapper method for coop_clear() */
extern void cpair_clear(void *self);
/** Copy CString object. It is a wrapper method for coop_copy() */
extern void *cpair_copy(const void *self);
/** Compares two CStrings. It is a wrapper method for coop_compare() */
extern int cpair_compare(const void *self, const void *second);
/** Converts the CString to a string or object. It is a wrapper method for coop_toString() */
extern char *cpair_toString(const void *self);
/** Outputs the CString text to the console. It is a wrapper method for coop_cout() */
extern void cpair_cout(const void *self);

// Methods only for CPair

/**
 * Get the first element
 * @param self, pointer to class data
 * @return object or NULL
 */
class_storeage_u cpair_first(const void *self);

/**
 * Get the second element
 * @param self, pointer to class data
 * @return object or NULL
 */
class_storeage_u cpair_second(const void *self);

/**
 * Check if pair is empty, cleared!
 * @param self, pointer to class data
 * @return 0 if not empty, 1 if empty
 */
unsigned int cpair_empty(void *self);

#endif /* C_PAIR_H_ */
