/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 */

#include <assert.h>

#include <string.h> // strlen(), strcpy()
#include <stdlib.h> // malloc()

#include "../Class/new.h"

#include "cstring.h"

// pre declaration of all methods

// Class methods
static void *String_ctor(void *self, unsigned int numargs, va_list *app);
static void *String_dtor(void *self);
static size_t String_size(const void *self);
static int String_assign(void *self, unsigned int numargs, va_list *app);
static void String_clear(void *self);
static void *String_copy(const void *self);
static int String_compare(void const *self, void const *second);
static char *String_to_string(const void *self);
static void String_cout(const void *self);

// Data type methods
static int String_append(void *self, unsigned int numargs, va_list *app);

// String methods
static void *String_substr(void *self, unsigned int index, int length);
static int String_find(const void *self, const char *str, size_t pos);

struct ClassString
{
  const struct ClassDataType class; /* Base class methods must be first */

  /* Additional methods */
  void *(*substr)(void *self, unsigned int index, int length);
  int (*find)(const void *self, const char *str, size_t pos);
};

/**
 * Define new type CStringClass
 * Size is the size of this data structure, size of the class CString.
 * The next function pointers assign the construct, destructor, ...
 */
static const struct ClassString CStringClass = {
    /* Attributes and methods of Class and CDataType */
    {
        .class.sizeOf = sizeof(struct CString),

        .class.ctor = String_ctor,
        .class.dtor = String_dtor,
        .class.size = String_size,
        .class.assign = String_assign,
        .class.clear = String_clear,
        .class.copy = String_copy,
        .class.compare = String_compare,
        .class.to_string = String_to_string,
        .class.cout = String_cout,

        .append = String_append,
    },

    /* Methods of class CString */
    .substr = String_substr,
    .find = String_find,
};

/** This is the memory of the class data (not of the object data) */
const void *CString = &CStringClass;

// Constructor of the string class
//
// The constructor retrieves the text passed to new() and stores a dynamic copy in the struct String which was allocated by new().
// In the constructor we only need to initialize .text because new() has already set up .class.
// app: arguments, here the char array with text
static void *String_ctor(void *self, unsigned int numargs, va_list *app)
{
  struct CString *csp = self;
  assert(numargs == 1);
  const char *text = va_arg(*app, const char *);

  csp->text = malloc(strlen(text) + 1);
  assert(csp->text);

  strcpy(csp->text, text);
  return csp;
}

// Destructor of the string class.
// The destructor frees the dynamic memory controlled by the string.
// Since delete() can only call the destructor if self is not null, we do not need to check things
static void *String_dtor(void *self)
{
  struct CString *csp = self;

  free(csp->text);
  csp->text = NULL;

  return csp;
}

// Returns the number of characters of the string.
static size_t String_size(const void *self)
{
  const struct CString *csp = self;
  if (csp->text != NULL)
    return strlen(csp->text);
  else
    return 0;
}

// Assign a new string to the CSTring object
static int String_assign(void *self, unsigned int numargs, va_list *app)
{
  struct CString *csp = self;
  const char *text = va_arg(*app, const char *);

  if (numargs == 1)
  {
    free(csp->text);

    csp->text = calloc(1, strlen(text) + 1);
    assert(csp->text);

    strcpy(csp->text, text);
    return NO_ERROR;
  }
  else
  {
    return CSTRING_INVALID_ARGUMENTS;
  }
}

// Clear string object
static void String_clear(void *self)
{
  struct CString *csp = self;
  free(csp->text);
  csp->text = NULL;
}

// Copy string object.
// String_clone() makes a copy of a string. Later both, the original and the copy,
// will be passed to delete() so we must make a new dynamic copy of the string’s
// text. This can easily be done by calling new().
static void *String_copy(const void *self)
{
  const struct CString *csp = self;
  return new (CString, csp->text);
}

// Compares the both strings of the objects
//   0: is both strings are equal
//  <0: self is lower than second
//  >0: self if higher than second
static int String_compare(void const *self, void const *second)
{
  const struct CString *csp = self;
  const struct CString *sec = second;

  return strcmp(csp->text, sec->text);
}

// Returns the text of the object
static char *String_to_string(const void *self)
{
  const struct CString *csp = self;
  char *p = malloc(strlen(csp->text) + 1);
  if (p != NULL)
    strcpy(p, csp->text);

  return p;
}

// Output text to console
static void String_cout(const void *self)
{
  const struct CString *csp = self;
  puts(csp->text);
}

// Append a string to the given CString object
static int String_append(void *self, unsigned int numargs, va_list *app)
{
  struct CString *csp = self;
  const char *text = va_arg(*app, const char *);
  size_t current_size = strlen(csp->text);
  size_t text_size = strlen(text);

  if (numargs == 1)
  {
    csp->text = realloc(csp->text, current_size + text_size + 1);
    assert(csp->text);
    strcat(csp->text, text);

    return NO_ERROR;
  }
  else
  {
    return CSTRING_INVALID_ARGUMENTS;
  }
}

// Get a sub string from given CString object
static void *String_substr(void *self, unsigned int index, int length)
{
  struct CString *csp = self;

  if (length < 0)
    length = strlen(csp->text) - index;

  char *p = calloc(1, length + 1);
  strncpy(p, csp->text + index, length);
  p[length] = '\0';
  void *sub = new (CString, p);

  free(p);

  return sub;
}

// Find a string into CString object.
// Note: The first character is denoted by a value of 0 (not 1): A value of 0 means that the entire string is searched.
//
// str: Another string with the subject to search for.
// pos: Position of the first character in the string to be considered in the search. If this is greater than the string length, the function never finds matches.
//
// Returns the position of the first character of the first match.
// If no matches were found, the function returns -1.
static int String_find(const void *self, const char *str, size_t pos)
{
  const struct CString *csp = self;
  char *p = strstr(csp->text + pos, str);
  return p == NULL ? -1 : p - csp->text;
}

//###########################################################################################
// Wrapper metods for Class and ClassDataType
//###########################################################################################

// Wrapper for Class
inline size_t cstr_sizeOf(const void *self) { return coop_sizeOf(self); }
inline size_t cstr_size(const void *self) { return coop_size(self); }
inline void cstr_clear(void *self) { coop_clear(self); }
inline void *cstr_copy(const void *self) { return coop_copy(self); }
inline int cstr_compare(const void *self, const void *second) { return coop_compare(self, second); }
inline char *cstr_toString(const void *self) { return coop_toString(self); }
inline void cstr_cout(const void *self) { coop_cout(self); }

// Wrapper for ClassDataType

//###########################################################################################
// Access class methods over this methods -> check pointers, ...
//###########################################################################################

// Get a sub string from given CString object
void *cstr_substring(void *self, unsigned int index, int length)
{
  const struct ClassString *const *cp = self;
  assert(self && *cp && (*cp)->substr);

  return (*cp)->substr(self, index, length);
}

// Find a string into CString object.
int cstr_find(const void *self, const char *str, size_t pos)
{
  const struct ClassString *const *cp = self;
  assert(self && *cp && (*cp)->find);
  return (*cp)->find(self, str, pos);
}
