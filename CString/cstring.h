/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 */

#ifndef C_STRING_H_
#define C_STRING_H_

#include "../Class/c_data_type.h"

enum { CSTRING_ERROR_ID = 0x0043537472696e67 };  // CString in hex

/**
 * Define error type for CString class
 */
typedef enum {
    CSTRING_NO_ERROR          = NO_ERROR,
    CSTRING_INVALID_ARGUMENTS = ( (CSTRING_ERROR_ID & 0x00FFFFFF) << 8 | 0x80000001 ),
} cstring_error_t;

/**
 * The string class.
 * Is derived from ClassDataType.
 * It contains the pointer to base class.
 * And is contains a pointer to the object data, here the text string.
 */
struct CString
{
  const void * ClassDataType; //!< Pointer to base class. Must be first
  char * text;                //!< Pointer to memory with the text
};

/** Pointer to the string class memory */
extern const void *CString;

// Wrapper for Class

/** Gets the size of the object memory. It is a wrapper method for coop_sizeOf() */
extern size_t cstr_sizeOf(const void * self);
/** Returns the length of the string. It is a wrapper method for coop_length() */
extern size_t cstr_size(const void * self);
/** Assign new string to object. It is a wrapper macro for coop_assign() */
#define cstr_assign(self, ... ) coop_assign_(self, NARGS(__VA_ARGS__), __VA_ARGS__)
/** Clear string of CString. It is a wrapper method for coop_clear() */
extern void cstr_clear(void * self);
/** Copy CString object. It is a wrapper method for coop_copy() */
extern void * cstr_copy( const void * self);
/** Compares two CStrings. It is a wrapper method for coop_compare() */
extern int cstr_compare(const void * self, const void * second);
/** Converts the CString to a string or object. It is a wrapper method for coop_toString() */
extern char * cstr_toString(const void * self);
/** Outputs the CString text to the console. It is a wrapper method for coop_cout() */
extern void cstr_cout(const void * self);

// Wrapper for ClassDataType

/** Append new string to given CString object. It is a wrapper macro for coop_append() */
#define cstr_append(self, ... ) coop_append_(self, NARGS(__VA_ARGS__), __VA_ARGS__)

// Methods only for CString

/** This macro ist used by cstr_substr() to calculate the number of arguments */
#define DEF_OR_ARG(value,...) value
/** A macros that creates a default parameter for cstr_substr(). It calls cstr_substring() with the passed or the default parameter. @see cstr_substring() */
#define cstr_substr(self, index, ... ) cstr_substring( self, index, DEF_OR_ARG(__VA_ARGS__ __VA_OPT__(,) -1) )

/**
 * Get a sub string from given CString object
 * Bad trick:
 * Preprocessor ## extension on variadic macro that allows you to simulate a default argument.
 * The trick has limitations: it works only for 1 default value, and the argument must be the last of you function parameters.
 * @see https://stackoverflow.com/questions/2988038/default-values-on-arguments-in-c-functions-and-function-overloading-in-c
 * 
 * @param self, pointer to class data
 * @param index, index of first char
 * @param length, number of chars, by < 0 gets from index to the end of string
 * @return Pointer to new CString object
 */
void * cstr_substring(void * self, unsigned int index, int length);

/**
 * Find a string into CString object.
 * Note: The first character is denoted by a value of 0 (not 1): A value of 0 means that the entire string is searched.
 *
 * str: Another string with the subject to search for.
 * pos: Position of the first character in the string to be considered in the search. If this is greater than the string length, the function never finds matches.
 * 
 * Returns the position of the first character of the first match.
 * If no matches were found, the function returns -1.
 * 
 * @param self, pointer to class data
 * @param str, Another string with the subject to search for.
 * @param pos, Position of the first character in the string to be considered in the search. 
 *             If this is greater than the string length, the function never finds matches. 
 * @return Returns the position of the first character of the first match. If no matches were found, the function returns -1. 
 */
int cstr_find(const void * self, const char *str, size_t pos);

#endif /* C_STRING_H_ */
