/*
 * Copyright (c) 2020 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>   // free
#include <string.h>

#include "../CList/clist.h"
#include "../CString/cstring.h"
#include "../CPair/cpair.h"
#include "../Class/new.h"

#include "minunit.h"

static const char *pTestString1 = "first text";
static const char *pTestString2 = "second text";
static const char *pTestString3 = "Servus Bayern";
static const char *pTestString4 = "UPPs";

static class_type_e type[] = { COOP_CHAR, COOP_INT, COOP_LONG, COOP_FLOAT, COOP_DOUBLE };
static class_storeage_u store[] = {
    { .c = 'a' },
    { .i = 4711 }, 
    { .l = 47110815l },
    { .f = 3.14f },
    { .d = 3.14159265359 },
    { .v = "" },
  };

static int (*cmp[])(void const *, void const *) = {
    (int (*)(void const *, void const *))strcmp,
    coop_compare_long,
    coop_compare_int,
    coop_compare_float,
    coop_compare_double,
    cstr_compare,
};

static size_t max = sizeof(type)/sizeof(type[0]);

// check sizeOf class
static char *clist_test_check_sizeof()
{
    mu_output_function();

    void *list = new (CList, COOP_OBJECT, cstr_compare);
    //clist_cout(list);
    size_t size = clist_sizeOf(list);
    mu_assert("error, object pointer is null", list != NULL);
    mu_assert("error, wrong object size", size == sizeof(struct CList));

    delete (list);
    return 0;
}

// check size method. Initialize CList with a string object
static char *clist_test_check_size()
{
    char * result = "[first text]";
    unsigned int error = NO_ERROR;
    void *string1 = new (CString, pTestString1);

    mu_output_function();

    void *list = new (CList, COOP_OBJECT, cstr_compare);
    error = clist_assign(list, string1);
    //clist_cout(list);
    size_t size = clist_size(list);
    char *p = clist_toString(list);

    mu_assert("error, object pointer is null", list != NULL);
    mu_assert("error, error by assignment", error == NO_ERROR);
    mu_assert("error, wrong object size", size == 1);
    mu_assert("error, invalid text", strcmp(p, result) == 0);

    delete (list);
    return 0;
}

// check init of CList with assign
static char * clist_test_init_assign() {
  void * string = new (CString, pTestString1);
  store[max].v = string;
  void * list = NULL;
  size_t arguments = 6;

  mu_output_function();
  for (unsigned int i = 0; i < max ; i++)
  {
    list = new (CList, type[i], cmp[i]);
    clist_assign(list, store[i], store[i], store[i], store[i], store[i], 'Z');

    //clist_cout(list);

    size_t size = clist_size(list);

    mu_assert("error, object pointer is null", list != NULL);
    mu_assert("error, list size wrong", size == arguments);

    delete (list);
  }

  // for string object
  list = new (CList, COOP_OBJECT, cstr_compare);
  clist_assign(list, string, string, string, string, string, string);
  
  //clist_cout(list);

  size_t size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == arguments);

  delete (list);
  delete (string);

  return 0;
}

// check clear of CList with 2 CPair objects
static char * clist_text_clear()
{
  void * string1 = new (CString, pTestString1);
  void * string2 = new (CString, pTestString2);
  void * pair = new (CPair, COOP_OBJECT, string1, cstr_compare, COOP_OBJECT, string2, cstr_compare);
  void * list = NULL;
  size_t size = 0;

  mu_output_function();
  //clist_cout(pair);

  list = new (CList, COOP_OBJECT, cstr_compare);
  clist_assign(list, pair, pair);

  //clist_cout(list);

  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 2);

  clist_clear(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 0);

  //clist_cout(list);

  delete(list);
  delete(pair);
  delete(string1);
  delete(string2);

  return 0;
}

// check copy and compare of CList with 2 different CPair objects
static char * clist_text_copy()
{
  void * string1 = new (CString, pTestString1);
  void * string2 = new (CString, pTestString2);
  void * pair1 = new (CPair, COOP_OBJECT, string1, cstr_compare, COOP_OBJECT, string2, cstr_compare);
  void * pair2 = new (CPair, COOP_OBJECT, string2, cstr_compare, COOP_OBJECT, string1, cstr_compare);
  void * list1 = NULL;
  void * list2 = NULL;
  void * list3 = NULL;
  size_t size = 0;
  unsigned int equal = 0;

  mu_output_function();
  /*clist_cout(pair1);
  clist_cout(pair2);*/

  list1 = new (CList, COOP_OBJECT, cstr_compare);
  clist_assign(list1, pair1);
  //clist_cout(list1);
  size = clist_size(list1);
  mu_assert("error, object pointer is null", list1 != NULL);
  mu_assert("error, list size wrong", size == 1);

  list2 = new (CList, COOP_OBJECT, cstr_compare);
  clist_assign(list2, pair1, pair2);
  //clist_cout(list2);
  size = clist_size(list2);
  mu_assert("error, object pointer is null", list2 != NULL);
  mu_assert("error, list size wrong", size == 2);

  // now copy list2
  list3 = clist_copy(list2);
  //clist_cout(list3);
  size = clist_size(list2);

  // list2 and list3 are equal
  equal = clist_compare(list2, list3);
  mu_assert("error, object pointer is null", list2 != NULL);
  mu_assert("error, list size wrong", size == 2);
  mu_assert("error, both list are not equal", equal == 0);

  // list1 and list3 are NOT equal
  equal = clist_compare(list1, list3);
  mu_assert("error, both list are equal", equal != 0);

  delete(list1);
  delete(list2);
  delete(list3);
  delete(pair1);
  delete(pair2);
  delete(string1);
  delete(string2);

  return 0;
}

// check init of CList with push insert
static char * clist_test_insert() {
  void * string = new (CString, pTestString1);
  store[max].v = string;
  void * list = NULL;

  mu_output_function();
  for (unsigned int i = 0; i < max ; i++)
  {
    list = new (CList, type[i], cmp[i]);
    if( i < 3)
    {
      clist_insert(list, store[i], 0);
      clist_insert(list, 'A', 1);
      clist_insert(list, 'Z', 0);
    }
    else if (i < 4)
    {
      clist_insert(list, store[i], 0);
      clist_insert(list, store[i], 1);
      clist_insert(list, 1.6f, 0);
    }
    else if (i < 5)
    {
      clist_insert(list, store[i], 0);
      clist_insert(list, store[i], 1);
      clist_insert(list, 1.6, 0);
    }
    else
    {
      clist_insert(list, store[i], 0);
      clist_insert(list, store[i], 1);
      clist_insert(list, store[i], 0);
    }

    //clist_cout(list);

    size_t size = clist_size(list);

    mu_assert("error, object pointer is null", list != NULL);
    mu_assert("error, list size wrong", size == 3);

    delete (list);
  }

  // for string object
  list = new (CList, COOP_OBJECT, cstr_compare);
  clist_insert(list, string, 0 );
  clist_insert(list, string, 1 );
  clist_insert(list, string, 0 );
  
  //clist_cout(list);

  size_t size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 3);

  delete (list);
  delete (string);

  return 0;
}

// check erase of CList with c types
static char * clist_text_erase()
{
  void * list = NULL;
  size_t size = 0;
  clist_error_t error = NO_ERROR;

  mu_output_function();

  list = new (CList, COOP_CHAR, strcmp);
  clist_assign(list, 'a', 'b', 'c', 'd');
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 4);

  // test erase command, position 2 -> 3'th element
  error = clist_erase(list, 2);
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 3);
  mu_assert("error, element not erased", error == NO_ERROR);

  // test erase command, position 0 -> 1'th element
  error = clist_erase(list, 0);
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 2);
  mu_assert("error, element not erased", error == NO_ERROR);

  // test erase command, position 2 -> 3'th element -> POSITION error
  error = clist_erase(list, 2);
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 2);
  mu_assert("error, element not erased", error == CLIST_INVALID_POSITION);

  // test erase command, position 1 -> 2'th element, here the last element
  error = clist_erase(list, 1);
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 1);
  mu_assert("error, element not erased", error == NO_ERROR);

  delete(list);

  return 0;
}

// check get of CList with own classes
static char * clist_text_get_object()
{
  void * string1 = new (CString, pTestString1);
  void * string2 = new (CString, pTestString2);
  void * string3 = new (CString, pTestString3);
  void * pair1 = new (CPair, COOP_OBJECT, string1, cstr_compare, COOP_OBJECT, string1, cstr_compare);
  void * pair2 = new (CPair, COOP_OBJECT, string2, cstr_compare, COOP_OBJECT, string2, cstr_compare);
  void * pair3 = new (CPair, COOP_OBJECT, string1, cstr_compare, COOP_OBJECT, string3, cstr_compare);
  void * list = NULL;
  void * pair4 = NULL;
  size_t size = 0;

  mu_output_function();
  /*cpair_cout(pair1);
  cpair_cout(pair2);
  cpair_cout(pair3);*/

  list = new (CList, COOP_OBJECT, cstr_compare);
  clist_assign(list, pair1, pair2, pair3, pair1);
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 4);

  // get from position 2 -> 3'th element in list
  pair4 = clist_get(list, 2);
  //cpair_cout(pair4);
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, object pointer is null", pair4 != NULL);
  mu_assert("error, list size wrong", size == 4);
  mu_assert("error wrong element", cpair_compare(pair4, pair3) == 0);
  delete(pair4);

  // get last element
  pair4 = clist_get(list, size  - 1);
  //cpair_cout(pair4);
  //clist_cout(list);

  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, object pointer is null", pair4 != NULL);
  mu_assert("error, list size wrong", size == 4);
  mu_assert("error wrong element", cpair_compare(pair4, pair1) == 0);
  delete(pair4);

  // get first element, equal to last element
  pair4 = clist_get(list, 0);
  //cpair_cout(pair4);
  //clist_cout(list);

  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, object pointer is null", pair4 != NULL);
  mu_assert("error, list size wrong", size == 4);
  mu_assert("error wrong element", cpair_compare(pair4, pair1) == 0);
  delete(pair4);


  delete(list);
  delete(pair1);
  delete(pair2);
  delete(pair3);
  delete(string1);
  delete(string2);
  delete(string3);

  return 0;
}

// check get of CList with c types
static char * clist_text_get_c_type()
{
  void * list = NULL;
  size_t size = 0;
  char * p = NULL;

  mu_output_function();

  list = new (CList, COOP_CHAR, strcmp);
  clist_assign(list, 'a', 'b', 'c', 'd');
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 4);

  // get from position 2 -> 3'th element in list, it's 'c'
  p = clist_get(list, 2);
  /*printf("%c\n", *p);
  clist_cout(list);*/

  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, object pointer is null", p != NULL);
  mu_assert("error, list size wrong", size == 4);
  mu_assert("error, wrong element", strcmp("c", (char*)p) == 0);
  free(p);

  // get element from last position, it's 'd'
  p = clist_get(list, size - 1);
  /*printf("%c\n", *p);
  clist_cout(list);*/

  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, object pointer is null", p != NULL);
  mu_assert("error, list size wrong", size == 4);
  mu_assert("error, wrong element", strcmp("d", (char*)p) == 0);
  free(p);

  // get first element, it's 'a'
  p = clist_get(list, 0);
  /*printf("%c\n", *p);
  clist_cout(list);*/

  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, object pointer is null", p != NULL);
  mu_assert("error, list size wrong", size == 4);
  mu_assert("error, wrong element", strcmp("a", (char*)p) == 0);
  free(p);

  // end of test, delete list
  delete(list);

  return 0;
}

// check count of CList
static char * clist_text_count()
{
  void * string1 = new (CString, pTestString1);
  void * string2 = new (CString, pTestString2);
  void * string3 = new (CString, pTestString3);
  void * string4 = new (CString, pTestString4);

  void * list = NULL;
  size_t size = 0;
  unsigned int count = 0;

  mu_output_function();

  // check c type
  list = new (CList, COOP_CHAR, strcmp);
  clist_assign(list, 'a', 'b', 'c', 'd', 'a', 'd', 'a');
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 7);

  count = clist_count(list, 'a');
  mu_assert("error, wrong number of elements (count)", count == 3);

  count = clist_count(list, 'x');
  mu_assert("error, wrong number of elements (count)", count == 0);

  // end of test, delete list
  delete(list);

  // check object (string)
  list = new (CList, COOP_OBJECT, cstr_compare);
  clist_assign(list, string1, string2, string3, string3, string2, string1, string3);
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 7);

  count = clist_count(list, string1);
  mu_assert("error, wrong number of elements (count)", count == 2);

  count = clist_count(list, string4);
  mu_assert("error, wrong number of elements (count)", count == 0);

  // end of test, delete list
  delete(list);

  delete(string1);
  delete(string2);
  delete(string3);
  delete(string4);

  return 0;
}

// check find of CList with own classes
static char * clist_text_find_object()
{
  void * string1 = new (CString, pTestString1);
  void * string2 = new (CString, pTestString2);
  void * string3 = new (CString, pTestString3);
  void * pair1 = new (CPair, COOP_OBJECT, string1, cstr_compare, COOP_OBJECT, string1, cstr_compare);
  void * pair2 = new (CPair, COOP_OBJECT, string2, cstr_compare, COOP_OBJECT, string2, cstr_compare);
  void * pair3 = new (CPair, COOP_OBJECT, string1, cstr_compare, COOP_OBJECT, string3, cstr_compare);
  void * list = NULL;
  void * pair4 = NULL;
  size_t size = 0;

  mu_output_function();
  /*cout(pair1);
  cout(pair2);
  cout(pair3);*/

  list = new (CList, COOP_OBJECT, cstr_compare);
  clist_assign(list, pair1, pair2, pair3, pair1);
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 4);

  // get from position 2 -> 3'th element in list
  pair4 = clist_find(list, pair3);
  /*cout(pair4);
  cout(list);*/
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, object pointer is null", pair4 != NULL);
  mu_assert("error, list size wrong", size == 4);
  mu_assert("error, wrong element", clist_compare(pair3, pair4) == 0);

  delete(list);
  delete(pair1);
  delete(pair2);
  delete(pair3);
  delete(string1);
  delete(string2);
  delete(string3);

  return 0;
}

// check find of CList with c types
static char * clist_text_find_c_type()
{
  void * list = NULL;
  size_t size = 0;
  char * p = NULL;

  mu_output_function();

  list = new (CList, COOP_CHAR, strcmp);
  clist_assign(list, 'a', 'b', 'c', 'd');
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 4);

  p = clist_find(list, 'b');
  /*printf("%c\n", *p);
  cout(list);*/
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, object pointer is null", p != NULL);
  mu_assert("error, list size wrong", size == 4);
  mu_assert("error, wrong element", *p == 'b');

  // end of test, delete list
  delete(list);

  return 0;
}

// check init of CList with push front
static char * clist_test_init_push_front() {
  void * string = new (CString, pTestString1);
  store[max].v = string;
  void * list = NULL;

  mu_output_function();
  for (unsigned int i = 0; i < max ; i++)
  {
    list = new (CList, type[i], cmp[i]);
    if( i < 3)
    {
      clist_push_front(list, store[i]);
      clist_push_front(list, store[i]);
      clist_push_front(list, 'Z');
    }
    else if (i < 4)
    {
      clist_push_front(list, store[i]);
      clist_push_front(list, store[i]);
      clist_push_front(list, store[i]);
    }
    else if (i < 5)
    {
      clist_push_front(list, store[i]);
      clist_push_front(list, store[i]);
      clist_push_front(list, store[i]);
    }
    else
    {
      clist_push_front(list, store[i]);
      clist_push_front(list, store[i]);
      clist_push_front(list, store[i]);
    }

    //clist_cout(list);

    size_t size = clist_size(list);

    mu_assert("error, object pointer is null", list != NULL);
    mu_assert("error, list size wrong", size == 3);

    delete (list);
  }

  // for string object
  list = new (CList, COOP_OBJECT, cstr_compare);
  clist_push_front(list, string );
  clist_push_front(list, string );
  clist_push_front(list, string );
  
  //clist_cout(list);

  size_t size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 3);

  delete (list);
  delete (string);

  return 0;
}

// check init of CList with push front
static char * clist_test_init_push_back() {
  void * string = new (CString, pTestString1);
  store[max].v = string;
  void * list = NULL;

  mu_output_function();
  for (unsigned int i = 0; i < max ; i++)
  {
    list = new (CList, type[i], cmp[i]);
    if( i < 3)
    {
      clist_push_back(list, store[i]);
      clist_push_back(list, store[i]);
      clist_push_back(list, 'Z');
    }
    else if (i < 4)
    {
      clist_push_back(list, store[i]);
      clist_push_back(list, store[i]);
      clist_push_back(list, store[i]);
    }
    else if (i < 5)
    {
      clist_push_back(list, store[i]);
      clist_push_back(list, store[i]);
      clist_push_back(list, store[i]);
    }
    else
    {
      clist_push_back(list, store[i]);
      clist_push_back(list, store[i]);
      clist_push_back(list, store[i]);
    }

    //clist_cout(list);

    size_t size = clist_size(list);

    mu_assert("error, object pointer is null", list != NULL);
    mu_assert("error, list size wrong", size == 3);

    delete (list);
  }

  // for string object
  list = new (CList, COOP_OBJECT, cstr_compare);
  clist_push_back(list, string );
  clist_push_back(list, string );
  clist_push_back(list, string );
  
  //clist_cout(list);

  size_t size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 3);

  delete (list);
  delete (string);

  return 0;
}

// check pop front of CList with own classes
static char * clist_text_pop_front_object()
{
  void * string1 = new (CString, pTestString1);
  void * string2 = new (CString, pTestString2);
  void * string3 = new (CString, pTestString3);
  void * pair1 = new (CPair, COOP_OBJECT, string1, cstr_compare, COOP_OBJECT, string1, cstr_compare);
  void * pair2 = new (CPair, COOP_OBJECT, string2, cstr_compare, COOP_OBJECT, string2, cstr_compare);
  void * pair3 = new (CPair, COOP_OBJECT, string1, cstr_compare, COOP_OBJECT, string3, cstr_compare);
  void * list = NULL;
  void * pair4 = NULL;
  size_t size = 0;

  mu_output_function();
  /*cpair_cout(pair1);
  cpair_cout(pair2);
  cpair_cout(pair3);*/

  list = new (CList, COOP_OBJECT, cstr_compare);
  clist_assign(list, pair1, pair2, pair3, pair1);
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 4);

  // pop from begin of list
  pair4 = clist_pop_front(list);
  //cpair_cout(pair4);
  //clist_cout(list);

  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, object pointer is null", pair4 != NULL);
  mu_assert("error, list size wrong", size == 3);
  delete(pair4);


  delete(list);
  delete(pair1);
  delete(pair2);
  delete(pair3);
  delete(string1);
  delete(string2);
  delete(string3);

  return 0;
}

// check pop front of CList with c types
static char * clist_text_pop_front_c_type()
{
  void * list = NULL;
  size_t size = 0;
  char * p = NULL;

  mu_output_function();

  list = new (CList, COOP_CHAR, strcmp);
  clist_assign(list, 'a', 'b', 'c', 'd');
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 4);

  // pop from begin of list
  p = clist_pop_front(list);
  /*printf("%c\n", *p);
  clist_cout(list);*/

  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, object pointer is null", p != NULL);
  mu_assert("error, list size wrong", size == 3);
  free(p);

  // end of test, delete list
  delete(list);

  return 0;
}

// check pop front of CList with own classes
static char * clist_text_pop_back_object()
{
  void * string1 = new (CString, pTestString1);
  void * string2 = new (CString, pTestString2);
  void * string3 = new (CString, pTestString3);
  void * pair1 = new (CPair, COOP_OBJECT, string1, cstr_compare, COOP_OBJECT, string1, cstr_compare);
  void * pair2 = new (CPair, COOP_OBJECT, string2, cstr_compare, COOP_OBJECT, string2, cstr_compare);
  void * pair3 = new (CPair, COOP_OBJECT, string1, cstr_compare, COOP_OBJECT, string3, cstr_compare);
  void * list = NULL;
  void * pair4 = NULL;
  size_t size = 0;

  mu_output_function();
  /*cpair_cout(pair1);
  cpair_cout(pair2);
  cpair_cout(pair3);*/

  list = new (CList, COOP_OBJECT, cstr_compare);
  clist_assign(list, pair1, pair2, pair3, pair1);
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 4);

  // pop from end of list
  pair4 = clist_pop_back(list);
  /*cpair_cout(pair4);
  clist_cout(list);*/

  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, object pointer is null", pair4 != NULL);
  mu_assert("error, list size wrong", size == 3);
  delete(pair4);

  delete(list);
  delete(pair1);
  delete(pair2);
  delete(pair3);
  delete(string1);
  delete(string2);
  delete(string3);

  return 0;
}

// check pop front of CList with c types
static char * clist_text_pop_back_c_type()
{
  void * list = NULL;
  size_t size = 0;
  char * p = NULL;

  mu_output_function();

  list = new (CList, COOP_CHAR, strcmp);
  clist_assign(list, 'a', 'b', 'c', 'd');
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 4);

  // pop from end of list
  p = clist_pop_back(list);
  /*printf("%c\n", *p);
  clist_cout(list);*/

  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, object pointer is null", p != NULL);
  mu_assert("error, list size wrong", size == 3);
  free(p);

  // end of test, delete list
  delete(list);

  return 0;
}

// check sort of CList with own classes
static char * clist_text_sort_object()
{
  char * result = "[Servus Bayern,first text,second text]";
  void * string1 = new (CString, pTestString1);
  void * string2 = new (CString, pTestString2);
  void * string3 = new (CString, pTestString3);
  void * list = NULL;
  size_t size = 0;

  mu_output_function();

  list = new (CList, COOP_OBJECT, cstr_compare);
  clist_assign(list, string1, string2, string3);
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 3);

  // now check method "sorts list"
  clist_sort(list);
  //clist_cout(list);
  size = clist_size(list);
  char *p = clist_toString(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 3);
  mu_assert("error, wrong sort", strcmp(result, p) == 0);

  delete(list);
  delete(string1);
  delete(string2);
  delete(string3);

  return 0;
}

// check sort of CList with c types
static char * clist_text_sort_c_type()
{
  char * result = "[a,d,f,z]";
  void * list = NULL;
  size_t size = 0;

  mu_output_function();

  list = new (CList, COOP_CHAR, strcmp);
  clist_assign(list, 'z', 'a', 'f', 'd');
  //clist_cout(list);
  size = clist_size(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 4);

  // now check method "sorts list"
  clist_sort(list);
  //clist_cout(list);
  size = clist_size(list);
  char *p = clist_toString(list);
  mu_assert("error, object pointer is null", list != NULL);
  mu_assert("error, list size wrong", size == 4);
  mu_assert("error, wrong sort", strcmp(result, p) == 0);
  
  // end of test, delete list
  delete(list);

  return 0;
}

// collect all unit tests of CList
static char * clist_all_tests() {
  mu_run_init();  /* must be first, clears the test run counter */

  mu_run_test(clist_test_check_sizeof);
  mu_run_test(clist_test_check_size);     // checks size() and to_string()
  mu_run_test(clist_test_init_assign);
  mu_run_test(clist_text_clear);
  mu_run_test(clist_text_copy);           // checks copy and compare

  mu_run_test(clist_test_insert);
  mu_run_test(clist_text_erase);
  mu_run_test(clist_text_get_object);
  mu_run_test(clist_text_get_c_type);
  mu_run_test(clist_text_count);
  mu_run_test(clist_text_find_object);
  mu_run_test(clist_text_find_c_type);

  mu_run_test(clist_test_init_push_front);
  mu_run_test(clist_test_init_push_back);
  mu_run_test(clist_text_pop_front_object);
  mu_run_test(clist_text_pop_front_c_type);
  mu_run_test(clist_text_pop_back_object);
  mu_run_test(clist_text_pop_back_c_type);
  mu_run_test(clist_text_sort_object);
  mu_run_test(clist_text_sort_c_type);

  return 0;
}

/**
 * Run all unit tests for CList class
 */
int ut_clist (void)
{  
  printf("Run unit tests for CList\n");
  char *result = clist_all_tests();
  if (result != 0) {
    printf("%s\n", result);
  }
  else {
    printf("\tALL TESTS PASSED\n");
  }
  printf("\tTests run: %d\n", tests_run);
 
  return result != 0;
}
