/* file: minunit.h */
/* Used from http://www.jera.com/techinfo/jtns/jtn002.html */
/*
 * Add two new macros: Init test (clears the counter and output current test function)
 */

#define mu_run_init() do { tests_run=0; } while (0)

#define mu_assert(message, test) do { if (!(test)) return message; } while (0)

#define mu_run_test(test) do { char *message = test(); tests_run++; if (message) return message; } while (0)

#define mu_output_function() do { printf ("\tRun unit test function: %s\n", __FUNCTION__); } while (0)

extern int tests_run;