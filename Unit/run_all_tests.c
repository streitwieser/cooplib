/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>

// Counts the number of tests.
int tests_run = 0;

extern int ut_cstring (void);
extern int ut_cpair (void);
extern int ut_cset (void);
extern int ut_clist (void);

int main(void)
{
  int result = 0;
  int tests_run_total = 0;

  /* Runs all unit tests for CString class */
  result = ut_cstring();
  tests_run_total += tests_run;
  
  /* Runs all unit test for CPair class */
  result = ut_cpair();
  tests_run_total += tests_run;

  /* Runs all unit test for CSet class */
  result = ut_cset();
  tests_run_total += tests_run;

  /* Runs all unit test for CList class */
  result = ut_clist();
  tests_run_total += tests_run;

  /* Total runs */
  printf("Total tests run: %d\n", tests_run_total);
  return result;  
}
