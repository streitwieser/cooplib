/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>   // free
#include <string.h>

#include "minunit.h"

#include "../CPair/cpair.h"
#include "../CString/cstring.h"
#include "../Class/new.h"

static const char *pTestString1 = "first text";
static const char *pTestString2 = "second text";

// check init of CPair with valid C types (char, int, long, float, double, object)
static char * cpair_test_init() {
  void * string1 = new (CString, pTestString1);
  class_type_e type[] = { COOP_CHAR, COOP_INT, COOP_LONG, COOP_FLOAT, COOP_DOUBLE, COOP_OBJECT };
  class_storeage_u store[] = {
    { .c = 'a' },
    { .i = 4711 }, 
    { .l = 47110815l },
    { .f = 3.14f },
    { .d = 3.14159265359 },
    { .v = string1 }
  };
  int (*cmp[])(void const *, void const *) = {
    (int (*)(void const *, void const *))strcmp,
    coop_compare_long,
    coop_compare_int,
    coop_compare_double,
    coop_compare_float,
    cstr_compare
  };

  size_t max = sizeof(type)/sizeof(type[0]);
  void * pair = NULL;

  mu_output_function();
  for (unsigned int i = 0; i < max; i++)
  {
    for (unsigned int j = 0; j < max; j++)
    {
      pair = new (CPair, type[i], store[i], cmp[i], type[j], store[j], cmp[j]);
      //coop_cout(pair);
      mu_assert("error, object pointer is null", pair != NULL);
      delete (pair);
    }
  }

  delete(string1);
  return 0;
}

// check sizeOf class
static char *cpair_test_check_sizeof()
{
  void *string1 = new (CString, pTestString1);
  void *string2 = new (CString, pTestString2);

  mu_output_function();

  void *pair = new (CPair, COOP_OBJECT, string1, cstr_compare, COOP_OBJECT, string2, cstr_compare);
  //coop_cout(pair);
  size_t size = cpair_sizeOf(pair);
  mu_assert("error, object pointer is null", pair != NULL);
  mu_assert("error, wrong object size", size == sizeof(struct CPair));

  delete (pair);
  delete(string1);
  delete(string2);
  return 0;
}

// check size method. Initialize CPair with two chars and check for correct initialization
static char * cpair_test_check_size() {
  void * result = "( a, A )";
  mu_output_function();

  void *pair = new (CPair, COOP_CHAR, 'a', strcmp, COOP_CHAR, 'A', strcmp);
  //coop_cout(pair);
  size_t size = cpair_size(pair);
  char *p = cpair_toString(pair);

  mu_assert("error, object pointer is null", pair != NULL);
  mu_assert("error, wrong object size", size == 2);
  mu_assert("error, invalid text", strcmp(p, result) == 0);

  delete (pair);
  return 0;
}

// check assign
static char * cpair_test_assign() {
  void * string1 = new (CString, pTestString1);
  void * string2 = new (CString, pTestString2);
  cpair_error_t error = NO_ERROR;

  mu_output_function();

  void * pair = new (CPair, COOP_OBJECT, string1, cstr_compare, COOP_OBJECT, string2, cstr_compare);
  //coop_cout(pair);

  // Check valid assing
  error = cpair_assign(pair, string2, string1);
  //coop_cout(pair);

  class_storeage_u p1 = cpair_first(pair);
  class_storeage_u p2 = cpair_second(pair);

  mu_assert("error, object pointer is null", pair != NULL);
  mu_assert("error, return value invalid", error == NO_ERROR);
  mu_assert("error, wrong first element", cstr_compare(string2, p1.v) == 0);
  mu_assert("error, wrong second element", cstr_compare(string1, p2.v) == 0);

  // Check invalid assing
  error = cpair_assign(pair, string2, string1, string1);
  //coop_cout(pair);
  mu_assert("error, object pointer is null", pair != NULL);
  mu_assert("error, return value invalid", error == CPAIR_NUMBER_ARGUMENTS);

  delete (pair);
  delete (p1.v);
  delete (p2.v);
  delete (string1);
  delete (string2);
  return 0;
}

// check compare, with 2 strings and copy
static char * cpair_test_compare_string() {
  void * string1 = new (CString, pTestString1);
  void * string2 = new (CString, pTestString2);
  int comp = 0;

  mu_output_function();

  void * pair1 = new (CPair, COOP_OBJECT, string1, cstr_compare, COOP_OBJECT, string2, cstr_compare);
  void * pair2 = new (CPair, COOP_OBJECT, string2, cstr_compare, COOP_OBJECT, string1, cstr_compare);
  void * pair3 = cpair_copy(pair1);

  /*coop_cout(pair1);
  coop_cout(pair2);
  coop_cout(pair3);*/

  mu_assert("error, object pointer is null", pair1 != NULL);
  mu_assert("error, object pointer is null", pair2 != NULL);
  mu_assert("error, object pointer is null", pair3 != NULL);

  comp = cpair_compare(pair1, pair1);
  mu_assert("error, pair1 and pair1 are NOT equal", comp == 0);

  comp = cpair_compare(pair1, pair2);
  mu_assert("error, pair1 and pair2 are equal", comp != 0);

  comp = cpair_compare(pair1, pair3);
  mu_assert("error, pair1 and pair3 are NOT equal", comp == 0 );

  delete (pair1);
  delete (pair2);
  delete (pair3);
  delete (string1);
  delete (string2);
  return 0;
}

// check compare, with different types
static char * cpair_test_compare_mix() {
  void * string = new (CString, pTestString1);
  class_type_e type[] = { COOP_CHAR, COOP_INT, COOP_LONG, COOP_FLOAT, COOP_DOUBLE, COOP_OBJECT };
  class_storeage_u store[] = {
    { .c = 'a' },
    { .i = 4711 }, 
    { .l = 47110815l },
    { .f = 3.14f },
    { .d = 3.14159265359 },
    { .v = string }
  };
  int (*cmp[])(void const *, void const *) = {
    (int (*)(void const *, void const *))strcmp,
    coop_compare_long,
    coop_compare_int,
    coop_compare_float,
    coop_compare_double,
    cstr_compare,
  };

  size_t max = sizeof(type)/sizeof(type[0]);
  void * pair1 = NULL;
  void * pair2 = NULL;
  int comp = 0;

  mu_output_function();

  for (unsigned int i = 0; i < max; i++)
  {
    for (unsigned int j = 0; j < max; j++)
    {
      for (unsigned int k = 0; k< max; k++)
      {
        for (unsigned int l = 0; l < max; l++)
        {
          pair1 = new (CPair, type[i], store[i], cmp[i], type[j], store[j], cmp[j] );
          pair2 = new (CPair, type[k], store[k], cmp[k], type[l], store[l], cmp[l] );
          mu_assert("error, object pointer is null", pair1 != NULL);
          mu_assert("error, object pointer is null", pair2 != NULL);

          /*coop_cout(pair1);
          coop_cout(pair2);*/

          comp = cpair_compare(pair1, pair2);
          if( (i == k) && (j == l) )
            mu_assert("error, object is not equal (compare)", comp == 0);
          else
            mu_assert("error, object is equal (compare)", comp != 0);

          delete (pair1);
          delete (pair2);
        }
      }
    }
  }

  delete(string);
  return 0;
}

// check toString
static char * cpair_test_to_string() {
  void * result = "( first text, second text )";
  void * string1 = new (CString, pTestString1);
  void * string2 = new (CString, pTestString2);

  mu_output_function();

  void * pair = new (CPair, COOP_OBJECT, string1, cstr_compare, COOP_OBJECT, string2, cstr_compare);
  //coop_cout(pair);
  char *p = cpair_toString(pair);
  mu_assert("error, object pointer is null", pair != NULL);
  mu_assert("error, text pointer invalid", p != NULL);
  mu_assert("error, invalid text", strcmp(p, result) == 0);

  free(p);
  delete (pair);
  delete (string1);
  delete (string2);
  return 0;
}

// check access
static char * cpair_test_access() {
  void * string1 = new (CString, pTestString1);
  void * string2 = new (CString, pTestString2);

  mu_output_function();

  void * pair = new (CPair, COOP_OBJECT, string1, cstr_compare, COOP_OBJECT, string2, cstr_compare);
  //coop_cout(pair);

  class_storeage_u p1 = cpair_first(pair);
  class_storeage_u p2 = cpair_second(pair);

  mu_assert("error, object pointer is null", pair != NULL);
  mu_assert("error, wrong first element", coop_compare(string1, p1.v) == 0);
  mu_assert("error, wrong second element", coop_compare(string2, p2.v) == 0);

  delete (pair);
  delete (p1.v);
  delete (p2.v);
  delete (string1);
  delete (string2);
  return 0;
}

// check empty and clear
static char * cpair_test_empty() {
  void * string1 = new (CString, pTestString1);
  void * string2 = new (CString, pTestString2);
  cpair_error_t error = NO_ERROR;
  unsigned int empty = 0;

  mu_output_function();

  void * pair = new (CPair, COOP_OBJECT, string1, cstr_compare, COOP_OBJECT, string2, cstr_compare);
  empty = cpair_empty(pair);
  //coop_cout(pair);
  mu_assert("error, object pointer is null", pair != NULL);
  mu_assert("error, return value invalid", error == NO_ERROR);
  mu_assert("error, is empty", empty == 0);

  cpair_clear(pair);
  empty = cpair_empty(pair);
  //coop_cout(pair);

  mu_assert("error, object pointer is null", pair != NULL);
  mu_assert("error, return value invalid", error == NO_ERROR);
  mu_assert("error, is not empty", empty == 1);

  delete (pair);
  delete (string1);
  delete (string2);
  return 0;
}


// collect all unit tests of CPair
static char * cpair_all_tests() {
  mu_run_init();  /* must be first, clears the test run counter */

  mu_run_test(cpair_test_init);

  mu_run_test(cpair_test_check_sizeof);
  mu_run_test(cpair_test_check_size);
  mu_run_test(cpair_test_assign);
  mu_run_test(cpair_test_compare_string); // checks compare and copy
  mu_run_test(cpair_test_compare_mix);
  mu_run_test(cpair_test_to_string);

  mu_run_test(cpair_test_access);
  mu_run_test(cpair_test_empty);    // checks empty and clear

  return 0;
}

/**
 * Run all unit tests for Cpair class
 */
int ut_cpair (void)
{  
  printf("Run unit tests for CPair\n");
  char *result = cpair_all_tests();
  if (result != 0) {
    printf("%s\n", result);
  }
  else {
    printf("\tALL TESTS PASSED\n");
  }
  printf("\tTests run: %d\n", tests_run);
 
  return result != 0;
}
