/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>   // free
#include <string.h>

#include "minunit.h"

#include "../Class/new.h"
#include "../Class/class.h"
#include "../Class/c_data_type.h"
#include "../CString/cstring.h"

static const char *pTestString1 = "first text";
static const char *pTestString2 = "second text";
static const char *pTestString3 = "first text second text";


// check init of CString
static char * cstring_test_init() {
    mu_output_function();

    void * a = new (CString, pTestString1);  
    mu_assert("error, object pointer is null", a != NULL);

    delete (a);
    return 0;
}

// check sizeOf class
static char * cstring_test_check_sizeof() {
    mu_output_function();

    void * a = new (CString, pTestString1);
    size_t size = cstr_sizeOf(a);

    mu_assert("error, object pointer is null", a != NULL);
    mu_assert("error, wrong object size", size == sizeof(struct CString) );

    delete (a);
    return 0;
}

// check method size of CString
static char * cstring_test_size() {
    mu_output_function();
    
    void * a = new (CString, pTestString1);
    size_t length = cstr_size(a);

    mu_assert("error, object pointer is null", a != NULL);
    mu_assert("error, wrong string length", length == strlen(pTestString1) );

    delete (a);
    return 0;
}

// check method assign
static char * cstring_test_assign() {
    mu_output_function();

    void * a = new (CString, pTestString1);
    cstr_assign(a, pTestString2);
    char *p = cstr_toString(a);
  
    mu_assert("error, object pointer is null", a != NULL);
    mu_assert("error, char pointer is null", p != NULL);
    mu_assert("error, invalid text", strcmp(p, pTestString2) == 0 );

    free(p);
    delete (a);
    return 0;
}

// check clear of CString
static char * cstring_test_clear() {
    size_t length = 0;
    void * a = NULL;

    mu_output_function();

    a = new (CString, pTestString1);  
    length = cstr_size(a);
    mu_assert("error, object pointer is null", a != NULL);
    mu_assert("error, wrong string length", length == strlen(pTestString1) );

    cstr_clear(a);
    length = cstr_size(a);
    mu_assert("error, object pointer is null", a != NULL);
    mu_assert("error, wrong string length", length == 0 );

    delete (a);
    return 0;
}

// check method copy and use cstr_compare
// The copy function creates a new object but with identical data, here with identical text.
static char * cstring_test_copy() {
    mu_output_function();

    void * a = new (CString, pTestString1);
    void *aa = cstr_copy (a);
  
    mu_assert("error, object pointer is null", a != NULL);
    mu_assert("error, is equal", cstr_compare(a, aa) == 0); // data is equal
    mu_assert("error, is a clone", a != aa);                // object is not equal

    delete (a);
    delete(aa);
    return 0;
}

// check method toString of CString
static char * cstring_test_to_string() {
    mu_output_function();

    void * a = new (CString, pTestString1);
    char *p = cstr_toString(a);

    mu_assert("error, object pointer is null", a != NULL);
    mu_assert("error, wrong string text", strcmp(p, pTestString1) == 0 );

    free(p);
    delete (a);
    return 0;
}

// check method append
static char * cstring_test_append() {
    mu_output_function();

    void * a = new (CString, pTestString1);
    cstr_append(a, " ");
    cstr_append(a, pTestString2);
    char *p = cstr_toString(a);
  
    mu_assert("error, object pointer is null", a != NULL);
    mu_assert("error, char pointer is null", p != NULL);
    mu_assert("error, invalid text", strcmp(p, pTestString3) == 0 );

    free(p);
    delete (a);
    return 0;
}

// check method cstr_substr
static char * cstring_test_substr() {
    mu_output_function();

    void * a = new (CString, pTestString1);

    // check index
    for(int index=0; index < (int)strlen(pTestString1); ++index)
    {
        void * b = cstr_substr(a, index);

        char *p1 = cstr_toString(a);
        char *p2 = cstr_toString(b);

        mu_assert("error, object pointer a is null", a != NULL);
        mu_assert("error, object pointer b is null", b != NULL);
        mu_assert("error, invalid text", strcmp(p1, pTestString1) == 0 );
        mu_assert("error, invalid sub string", strcmp(p2, pTestString1 + index) == 0 );

        free(p1);
        free(p2);
        delete (b);
    }

    // check length
    for(int index=0; index <= (int)strlen(pTestString1); ++index)
    {
        void * b = cstr_substr(a, 0, index);

        char *p1 = cstr_toString(a);
        char *p2 = cstr_toString(b);

        mu_assert("error, object pointer a is null", a != NULL);
        mu_assert("error, object pointer b is null", b != NULL);
        mu_assert("error, invalid text", strcmp(p1, pTestString1) == 0 );
        mu_assert("error, invalid sub string", strncmp(p2, pTestString1, index) == 0 );

        free(p1);
        free(p2);
        delete (b);
    }

    delete (a);
    return 0;
}

// check method find
static char * cstring_test_find() {
    void * a = new (CString, pTestString1);
    char *str1 = "text";
    char *str2 = "test";
    char *str3 = "first";
    int n = 0;

    mu_output_function();

    // find string "text" in "first text", start position 0
    n = cstr_find(a, str1, 0);
    mu_assert("error, string not found", n == 6 );

    // not find string "test" in "first text", start position 0
    n = cstr_find(a, str2, 0);
    mu_assert("error, string found!", n == -1 );

    // find string "first" in "first text", start position 0
    n = cstr_find(a, str3, 0);
    mu_assert("error, string not found", n == 0 );

    // not find string "first" in "first text", start position 1
    n = cstr_find(a, str3, 1);
    mu_assert("error, string found!", n == -1 );

    delete (a);
    return 0;
}

// collect all unit tests of CString
static char * cstring_all_tests() {
    mu_run_init();  /* must be first, clears the test run counter */

    // Class
    mu_run_test(cstring_test_init);
    mu_run_test(cstring_test_check_sizeof);
    mu_run_test(cstring_test_size);
    mu_run_test(cstring_test_assign);
    mu_run_test(cstring_test_clear);
    mu_run_test(cstring_test_copy);
    mu_run_test(cstring_test_to_string);

    // ClassDataType
    mu_run_test(cstring_test_append);

    // CString
    mu_run_test(cstring_test_substr);
    mu_run_test(cstring_test_find);
    
    return 0;
}

/**
 * Run all unit tests for CString class
 */
int ut_cstring (void)
{  
    printf("Run unit tests for CString\n");
    char *result = cstring_all_tests();
    if (result != 0) {
        printf("%s\n", result);
    }
    else {
        printf("\tALL TESTS PASSED\n");
    }
    printf("\tTests run: %d\n", tests_run);
 
    return result != 0;
}
