/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h> // free
#include <string.h>

#include "../Class/new.h"

#include "../CSet/cset.h"
#include "../CString/cstring.h"

#include "minunit.h"

static const char *pTestString1 = "first text";
static const char *pTestString2 = "second text";
static const char *pTestString3 = "third text";

// check init of CSet
static char *cset_test_init()
{
    mu_output_function();

    void *set = new (CSet, COOP_OBJECT, cstr_compare);
    mu_assert("error, object pointer is null", set != NULL);

    unsigned int size = cset_size(set);
    mu_assert("error, cset is not empty", size == 0);

    delete (set);
    return 0;
}

// check sizeOf class
static char *cset_test_check_sizeof()
{
    mu_output_function();

    void *set = new (CSet, COOP_OBJECT, cstr_compare);
    //coop_cout(set);
    size_t size = cset_sizeOf(set);
    mu_assert("error, object pointer is null", set != NULL);
    mu_assert("error, wrong object size", size == sizeof(struct CSet));

    delete (set);
    return 0;
}

// check size method. Initialize CPair with two chars and check for correct initialization
static char *cset_test_check_size()
{
    char * result = "(first text)";
    unsigned int error = NO_ERROR;
    void *string1 = new (CString, pTestString1);

    mu_output_function();

    void *set = new (CSet, COOP_OBJECT, cstr_compare);
    error = cset_assign(set, string1);
    //cset_cout(set);
    size_t size = cset_size(set);
    char *p = cset_toString(set);

    mu_assert("error, object pointer is null", set != NULL);
    mu_assert("error, error by assignment", error == NO_ERROR);
    mu_assert("error, wrong object size", size == 1);
    mu_assert("error, invalid text", strcmp(p, result) == 0);

    delete (set);
    return 0;
}

// check assign method, inset three different string objects
static char *cset_test_assign_string()
{
    //                    first string  third string  second string
    const char *pc[3] = {pTestString1, pTestString3, pTestString2};
    void *string[3] = {NULL, NULL, NULL};
    cset_error_t error = NO_ERROR;
    unsigned int size = 0;

    for (unsigned int index = 0; index < 3; ++index)
    {
        string[index] = new (CString, pc[index]);
    }

    mu_output_function();

    void *set = new (CSet, COOP_OBJECT, cstr_compare);

    // first assing, empty set
    error = cset_assign(set, string[0], string[1], string[2]);
    size = cset_size(set);
    //cset_cout(set);
    mu_assert("error, object not inserted", error == NO_ERROR);
    mu_assert("error, cset is empty", size == 3);

    // second assign, clears current set
    error = cset_assign(set, string[0], string[1], string[2]);
    size = cset_size(set);
    //cset_cout(set);
    mu_assert("error, object not inserted", error == NO_ERROR);
    mu_assert("error, cset is empty", size == 3);

    // assign error, twice
    error = cset_assign(set, string[0], string[1], string[2], string[0]);
    size = cset_size(set);
    //cset_cout(set);
    mu_assert("error, object inserted", error == CSET_NOT_UNIQUE);
    mu_assert("error, cset is empty", size == 3);

    delete (set);
    delete (string[2]);
    delete (string[1]);
    delete (string[0]);

    return 0;
}

// check assign method, assign three different c data types
static char *cset_test_assign_c_types()
{
    class_type_e type[] = {COOP_CHAR, COOP_INT, COOP_LONG, COOP_FLOAT, COOP_DOUBLE};
    class_storeage_u store[][3] = {
        {   {.c = 'a'}, {.c = 'b'}, {.c = 'c'}, },
        {   {.i = 4}, {.i = 2}, {.i = 0}, },
        {   {.l = 10}, {.l = 100}, {.l = 3}, },
        {   {.f = 3.14f}, {.f = 2.7f}, {.f = 1.6f}, },
        {   {.d = 3.14159265359}, {.d = 2.7}, {.d = 3.0} } };
    int (*cmp[])(void const *, void const *) = {
        (int (*)(void const *, void const *))strcmp,
        coop_compare_long,
        coop_compare_int,
        coop_compare_float,
        coop_compare_double,
    };
    size_t max = sizeof(type) / sizeof(type[0]);
    void *set = NULL;
    cset_error_t error = NO_ERROR;
    int size = 0;

    mu_output_function();

    for (unsigned int index = 0; index < max; ++index)
    {
        set = new (CSet, type[index], cmp[index]);

        error = cset_assign(set, store[index][0], store[index][1], store[index][2]);
        size = cset_size(set);
        //cset_cout(set);
        mu_assert("error, object not inserted", error == NO_ERROR);
        mu_assert("error, cset is empty", size == 3);

        delete (set);
    }

    return 0;
}

// check clear method
static char *cset_test_clear()
{
    void *string1 = new (CString, pTestString1);
    void *string2 = new (CString, pTestString2);
    cset_error_t error = NO_ERROR;
    unsigned int size = 0;

    mu_output_function();
    void *set = new (CSet, COOP_OBJECT, cstr_compare);

    error |= cset_insert(set, string1);
    error |= cset_insert(set, string2);
    size = cset_size(set);
    //cset_cout(set);

    mu_assert("error, objects not inserted", error == NO_ERROR);
    mu_assert("error, cset is empty", size == 2);

    cset_clear(set);
    //cset_cout(set);
    size = cset_size(set);
    mu_assert("error, cset is empty", size == 0);

    delete (set);
    delete (string2);
    delete (string1);

    return 0;
}

// check copy method
static char *cset_test_copy()
{
    const char *pc[3] = {pTestString1, pTestString2, pTestString3};
    void *string[3] = {NULL, NULL, NULL};
    int equal = 0;

    mu_output_function();
    void *set1 = new (CSet, COOP_OBJECT, cstr_compare);

    for (unsigned int index = 0; index < 3; ++index)
    {
        string[index] = new (CString, pc[index]);
        cset_insert(set1, string[index]);
        //cset_cout(set1);
    }

    void *set2 = cset_copy(set1);
    //cset_cout(set2);
    equal = cset_compare(set1, set2);
    mu_assert("error, both sets must be equal", equal == 0);

    delete (set1);
    delete (set2);
    delete (string[2]);
    delete (string[1]);
    delete (string[0]);

    return 0;
}

// check compare method
static char *cset_test_compare_string()
{
    const char *pc[3] = {pTestString1, pTestString2, pTestString3};
    void *string[3] = {NULL, NULL, NULL};
    int cmp = 0;

    mu_output_function();
    void *set1 = new (CSet, COOP_OBJECT, cstr_compare);
    void *set2 = new (CSet, COOP_OBJECT, cstr_compare);
    void *set3 = new (CSet, COOP_OBJECT, cstr_compare);
    void *set4 = new (CSet, COOP_OBJECT, cstr_compare);

    for (unsigned int index = 0; index < 3; ++index)
        string[index] = new (CString, pc[index]);

    for (unsigned int index = 0; index < 3; ++index)
    {
        cset_insert(set1, string[index]);
        cset_insert(set2, string[index]);

        if (index < 2)
            cset_insert(set3, string[index]);

        cset_insert(set4, string[2 - index]);
    }

    /*cset_cout(set1);
    cset_cout(set2);
    cset_cout(set3);
    cset_cout(set4);*/

    cmp = cset_compare(set1, set2);
    mu_assert("error, set1 and set2 are not equal", cmp == 0);

    cmp = cset_compare(set1, set3);
    mu_assert("error, set1 and set3 are eqaul", cmp != 0);

    cmp = cset_compare(set3, set2);
    mu_assert("error, set3 and set2 are eqaul", cmp != 0);

    // a set sorts the element data
    cmp = cset_compare(set1, set4);
    mu_assert("error, set1 and set4 are not eqaul", cmp == 0);

    delete (set1);
    delete (set2);
    delete (set3);
    delete (set4);
    delete (string[2]);
    delete (string[1]);
    delete (string[0]);

    return 0;
}

// check compare method, c type, float
static char *cset_test_compare_c_type()
{
    const float f[] = {1.0f, 15.6f, -1.3f};
    void *string[3] = {NULL, NULL, NULL};
    int cmp = 0;

    mu_output_function();
    void *set1 = new (CSet, COOP_FLOAT, coop_compare_float);
    void *set2 = new (CSet, COOP_FLOAT, coop_compare_float);
    void *set3 = new (CSet, COOP_FLOAT, coop_compare_float);
    void *set4 = new (CSet, COOP_FLOAT, coop_compare_float);

    for (unsigned int index = 0; index < 3; ++index)
    {
        cset_insert(set1, f[index]);
        cset_insert(set2, f[index]);

        if (index < 2)
            cset_insert(set3, f[index]);

        cset_insert(set4, f[2 - index]);
    }

    /*cset_cout(set1);
    cset_cout(set2);
    cset_cout(set3);
    cset_cout(set4);*/

    cmp = cset_compare(set1, set2);
    mu_assert("error, set1 and set2 are not equal", cmp == 0);

    cmp = cset_compare(set1, set3);
    mu_assert("error, set1 and set3 are equal", cmp != 0);

    cmp = cset_compare(set3, set2);
    mu_assert("error, set3 and set2 are equal", cmp != 0);

    // a set sorts the element data
    cmp = cset_compare(set1, set4);
    mu_assert("error, set1 and set4 are not equal", cmp == 0);

    delete (set1);
    delete (set2);
    delete (set3);
    delete (set4);
    delete (string[2]);
    delete (string[1]);
    delete (string[0]);

    return 0;
}

// check toString method
static char *cset_test_to_string()
{
    //                    first string  third string  second string
    const char *pc[3] = {pTestString1, pTestString3, pTestString2};
    const char *result = "(first text,second text,third text)";
    void *string[3] = {NULL, NULL, NULL};
    cset_error_t error = NO_ERROR;
    unsigned int size = 0;

    mu_output_function();
    void *set = new (CSet, COOP_OBJECT, cstr_compare);

    for (unsigned int index = 0; index < 3; ++index)
    {
        string[index] = new (CString, pc[index]);
        error = cset_insert(set, string[index]);
        //cset_cout(set);
        size = cset_size(set);
        mu_assert("error, object not inserted", error == NO_ERROR);
        mu_assert("error, cset is empty", size == index + 1);
    }

    char *text = cset_toString(set);
    mu_assert("error, wrong string", strcmp(text, result) == 0);
    free(text);

    delete (set);
    delete (string[2]);
    delete (string[1]);
    delete (string[0]);

    return 0;
}

// check inset method, inset three different string objects
static char *cset_test_insert_string()
{
    //                    first string  third string  second string
    const char *pc[3] = {pTestString1, pTestString3, pTestString2};
    void *string[3] = {NULL, NULL, NULL};
    cset_error_t error = NO_ERROR;
    unsigned int size = 0;

    mu_output_function();
    void *set = new (CSet, COOP_OBJECT, cstr_compare);

    for (unsigned int index = 0; index < 3; ++index)
    {
        string[index] = new (CString, pc[index]);
        error = cset_insert(set, string[index]);
        size = cset_size(set);
        //cset_cout(set);
        mu_assert("error, object not inserted", error == NO_ERROR);
        mu_assert("error, cset is empty", size == index + 1);
    }

    delete (set);
    delete (string[2]);
    delete (string[1]);
    delete (string[0]);

    return 0;
}

// check inset method, inset three different c data type
static char *cset_test_insert_c_types()
{
    class_type_e type[] = {COOP_CHAR, COOP_INT, COOP_LONG, COOP_FLOAT, COOP_DOUBLE};
    class_storeage_u store[][3] = {
        {   {.c = 'a'}, {.c = 'b'}, {.c = 'c'}, },
        {   {.i = 4}, {.i = 2}, {.i = 0}, },
        {   {.l = 10}, {.l = 100}, {.l = 3}, },
        {   {.f = 3.14f}, {.f = 2.7f}, {.f = 1.6f}, },
        {   {.d = 3.14159265359}, {.d = 2.7}, {.d = 3.0} } };
    int (*cmp[])(void const *, void const *) = {
        (int (*)(void const *, void const *))strcmp,
        coop_compare_long,
        coop_compare_int,
        coop_compare_float,
        coop_compare_double,
    };
    size_t max = sizeof(type) / sizeof(type[0]);
    void *set = NULL;
    cset_error_t error = NO_ERROR;
    int size = 0;

    mu_output_function();

    for (unsigned int index = 0; index < max; ++index)
    {
        set = new (CSet, type[index], cmp[index]);

        for (unsigned int i = 0; i < 3; i++)
        {
            error = cset_insert(set, store[index][i]);
            //cset_cout(set);
            mu_assert("error, object not inserted", error == NO_ERROR);
        }
        size = cset_size(set);
        mu_assert("error, cset is empty", size == 3);

        delete (set);
    }

    return 0;
}

// check inset method, try to inset same string twice
static char *cset_test_insert_twice()
{
    void *string1 = new (CString, pTestString1);
    cset_error_t error = NO_ERROR;
    unsigned int size = 0;

    mu_output_function();
    void *set = new (CSet, COOP_OBJECT, cstr_compare);

    error = cset_insert(set, string1);
    //cset_cout(set);
    size = cset_size(set);
    mu_assert("error, object not inserted", error == NO_ERROR);
    mu_assert("error, cset is empty", size == 1);

    error = cset_insert(set, string1);
    //cset_cout(set);
    size = cset_size(set);
    mu_assert("error, object inserted", error == CSET_NOT_UNIQUE);
    mu_assert("error, cset is empty", size == 1);

    delete (set);
    delete (string1);

    return 0;
}

// check erase method
static char *cset_test_erase()
{
    void *string1 = new (CString, pTestString1);
    void *string2 = new (CString, pTestString2);
    cset_error_t error = NO_ERROR;
    unsigned int size = 0;

    mu_output_function();
    void *set = new (CSet, COOP_CHAR, strcmp);

    error = cset_assign(set, 'Z', 'b', 'd', '@');
    //cset_cout(set);
    size = cset_size(set);
    mu_assert("error, objects not inserted", error == NO_ERROR);
    mu_assert("error, cset is empty", size == 4);

    error = cset_erase(set, 2); // erase 'b', it's the 3 element after sort (@,Z,b,d)
    //cset_cout(set);
    size = cset_size(set);
    mu_assert("error, objects not erased", error == NO_ERROR);
    mu_assert("error, cset is empty", size == 3);

    error = cset_erase(set, 0); // erase '@', it's the 1 element after sort (@,Z,d)
    //cset_cout(set);
    size = cset_size(set);
    mu_assert("error, objects not erased", error == NO_ERROR);
    mu_assert("error, cset is empty", size == 2);

    error = cset_erase(set, 1); // erase 'd', it's the 2 element after sort (Z,d)
    //cset_cout(set);
    size = cset_size(set);
    mu_assert("error, objects not erased", error == NO_ERROR);
    mu_assert("error, cset is empty", size == 1);

    delete (set);
    delete (string2);
    delete (string1);

    return 0;
}

// check get method
static char *cset_test_get()
{
    //                    first string  third string  second string
    const char *pc[3] = {pTestString1, pTestString3, pTestString2};
    void *string[3] = {NULL, NULL, NULL};
    cset_error_t error = NO_ERROR;
    unsigned int size = 0;

    mu_output_function();
    void *set = new (CSet, COOP_OBJECT, cstr_compare);

    for (unsigned int index = 0; index < 3; ++index)
    {
        string[index] = new (CString, pc[index]);
        error = cset_insert(set, string[index]);
        //cset_cout(set);
        size = cset_size(set);
        mu_assert("error, object not inserted", error == NO_ERROR);
        mu_assert("error, cset is empty", size == index + 1);
    }

    // set has sort the string objects
    void *s = cset_get(set, 2);
    mu_assert("error, wrong object", cset_compare(s, string[1]) == 0);

    delete (s);
    delete (set);
    delete (string[2]);
    delete (string[1]);
    delete (string[0]);

    return 0;
}

// check count method
static char *cset_test_count()
{
    char c[] = {'c', 'z', 'a'};
    cset_error_t error = NO_ERROR;
    unsigned int size = 0, count = 0;

    mu_output_function();
    void *set = new (CSet, COOP_CHAR, strcmp);

    for (unsigned int index = 0; index < 3; ++index)
    {
        error = cset_insert(set, c[index]);
        //cset_cout(set);
        size = cset_size(set);
        mu_assert("error, object not inserted", error == NO_ERROR);
        mu_assert("error, cset is empty", size == index + 1);
    }

    count = cset_count(set, 'z');

    mu_assert("error, object not inserted", error == NO_ERROR);
    mu_assert("error, cset is empty", size == 3);
    mu_assert("error, invalid count", count == 1);

    count = cset_count(set, 'T');
    mu_assert("error, invalid count", count == 0);

    delete (set);
    return 0;
}

// check find method, string object
static char *cset_test_find_string()
{
    //                    first string  third string  second string
    const char *pc[3] = {pTestString1, pTestString3, pTestString2};
    void *string[3] = {NULL, NULL, NULL};
    cset_error_t error = NO_ERROR;
    unsigned int size = 0;

    mu_output_function();
    void *set = new (CSet, COOP_OBJECT, cstr_compare);

    for (unsigned int index = 0; index < 3; ++index)
    {
        string[index] = new (CString, pc[index]);
        error = cset_insert(set, string[index]);
        //cset_cout(set);
        size = cset_size(set);
        mu_assert("error, object not inserted", error == NO_ERROR);
        mu_assert("error, cset is empty", size == index + 1);
    }

    // set has sort the string objects
    void *p = cset_find(set, string[1]);
    mu_assert("error, object pinter invalid", p != NULL);

    delete (set);
    delete (string[2]);
    delete (string[1]);
    delete (string[0]);

    return 0;
}

// check find method, c type char
static char *cset_test_find_c_type()
{
    char c[] = {'c', 'z', 'a'};
    cset_error_t error = NO_ERROR;
    unsigned int size = 0;

    mu_output_function();
    void *set = new (CSet, COOP_CHAR, strcmp);

    for (unsigned int index = 0; index < 3; ++index)
    {
        error = cset_insert(set, c[index]);
        //cset_cout(set);
        size = cset_size(set);
        mu_assert("error, object not inserted", error == NO_ERROR);
        mu_assert("error, cset is empty", size == index + 1);
    }

    // set has sort the string objects
    void *p = cset_find(set, 'z');
    mu_assert("error, object pinter invalid", p != NULL);

    delete (set);
    return 0;
}

// collect all unit tests of CSet
static char *cset_all_tests()
{
    mu_run_init(); /* must be first, clears the test run counter */

    mu_run_test(cset_test_init);
    mu_run_test(cset_test_check_sizeof);
    mu_run_test(cset_test_check_size);
    mu_run_test(cset_test_assign_string);
    mu_run_test(cset_test_assign_c_types);
    mu_run_test(cset_test_clear);
    mu_run_test(cset_test_copy);
    mu_run_test(cset_test_compare_string);
    mu_run_test(cset_test_compare_c_type);
    mu_run_test(cset_test_to_string);

    mu_run_test(cset_test_insert_string);
    mu_run_test(cset_test_insert_c_types);
    mu_run_test(cset_test_insert_twice);
    mu_run_test(cset_test_erase);
    mu_run_test(cset_test_get);
    mu_run_test(cset_test_count);
    mu_run_test(cset_test_find_string);
    mu_run_test(cset_test_find_c_type);

    return 0;
}

/**
 * Run all unit tests for CSet class
 */
int ut_cset(void)
{
    printf("Run unit tests for CSet\n");
    char *result = cset_all_tests();
    if (result != 0)
    {
        printf("%s\n", result);
    }
    else
    {
        printf("\tALL TESTS PASSED\n");
    }
    printf("\tTests run: %d\n", tests_run);

    return result != 0;
}
