/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 *
 * Note that constructor and destructor are not responsible for acquiring and
 * releasing the memory for an object itself — this is the job of new() and delete().
 * The constructor is called by new() and is only responsible for initializing the memory
 * area allocated by new(). For a string, this does involve acquiring another piece of
 * memory to store the text, but the space for struct String itself is allocated by
 * new(). This space is later freed by delete(). First, however, delete() calls the des-
 * tructor which essentially reverses the initialization done by the constructor before
 * delete() recycles the memory area allocated by new().
 *
 * Memory organization.
 *
 *                         Object                  Class
 *     ------------	    -----------------       ------------
 * p ->|     x    |---->| class pointer |------>| size	   |
 *     ------------	    -----------------       ------------
 *          			      | data pointers |       | ctor     |
 *     				    		                          ------------
 *     				    		                          | dtor     |
 *                     				              		------------
 *               				                    		| copy     |
 *                               				    		------------
 *                                              | equal    |
 *                                              ------------
 *     				                              		| cout     |
 *     				    		                          ------------
 *               				    		                |          |
 *
 * The type description class at the right is initialized at compile time.
 * The object is created at run time and the dashed pointers are then inserted.
 * In the assignment (see new())
 *  * (const struct Class **) p = class;
 * p points to the beginning of the new memory area for the object. We force a
 * conversion of p which treats the beginning of the object as a pointer to a struct
 * Class and set the argument class as the value of this pointer.
 */

#include <assert.h>
#include <stdlib.h>		// for calloc(), free()
#include <stdarg.h>

#include "class.h"

/*
 * This function allocate memory for the object and copy the class data memory (see first argument) to this memory -> now we have an object.
 * Afterwards it calls the constructor with the given arguments for this object.
 * The delete function has to free the memory.
 * The constructor of the class has to free the object memory (for example the memory of text data).
 *
 * Note that only explicitly visible functions like new() can have a variable parameter list.
 * The list is accessed with a va_list variable ap which is initialized using the macro va_start() from stdarg.h.
 * new() can only pass the entire list to the constructor; therefore,
 * .ctor is declared with a va_list parameter and not with its own variable parameter list.
 * Since we might later want to share the original parameters
 */
void * new_(const void * type, unsigned int numargs, ...)
{
  const struct Class * class = type;
  void * p = calloc(1, class->sizeOf);
  assert(p);

  *(const struct Class **) p = class;
  if (class->ctor)
  {
	  va_list ap;
	  va_start(ap, numargs);
	  p = class->ctor(p, numargs, &ap);
	  va_end(ap);
  }

  return p;
}

/*
 * First call the destructor of the class.
 * It frees the object memory -> for example the text buffer of the string class
 * Afterwards free the memory of the class data structure.
 *
 * delete() assumes that each object, i.e., each non-null pointer, points to a type
 * description. This is used to call the destructor if any exists. Here, self plays the
 * role of p in the above picture. We force the conversion using a local variable cp
 * and very carefully thread our way from self to its description:
 */
void delete(void * self)
{
  const struct Class ** cp = self;

  // If self pointer, self data and destructor not null then call destructor
  if (self && *cp && (*cp)->dtor)
    self = (*cp)->dtor(self);

  free (self);
}