/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 * 
 * This is the base class of all data types as CString, ....
 */

#ifndef CDATATYPE_H_
#define CDATATYPE_H_

#include "class.h"

/**
 * @brief Data structure of the class CDataType
 */
struct ClassDataType {
  const struct Class class;   /* Class methods must be first */

  /* Additional methods */
  int (*append)(void * self, unsigned int numargs, va_list * app );
};

/**
 * Append new elements to object
 * @param self, pointer to class data
 * @param numargs, number of variable arguments
 * @param args, an argument list with elements
 * @return error code
 */
#define coop_append(self, ... ) coop_append_(self, NARGS(__VA_ARGS__), __VA_ARGS__)
int coop_append_(void * self, unsigned int numargs, ... );

#endif // CDATATYPE_H_
