/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 */

#include <assert.h>
#include "c_container.h"

// Insert new object
int coop_insert_(void * self, unsigned int numargs, ... )
{
  const struct ClassContainer * const * cp = self;
  assert(self && *cp && (*cp)->insert);

  va_list ap;
  va_start(ap, numargs);
  int error = (*cp)->insert(self, numargs, &ap );
  va_end(ap);

  return error;
}

// Erase element from container on given position
int coop_erase(void * self, unsigned int pos)
{
  const struct ClassContainer * * cp = self;
  assert(self && *cp && (*cp)->erase);
  return (*cp)->erase(self, pos);
}

// Get the object on position pos.
void * coop_get(const void * self, size_t pos)
{
  const struct ClassContainer * const * cp = self;
  assert(self && *cp && (*cp)->get);
  return (*cp)->get(self, pos);
}

// Count number of given element
unsigned int coop_count_(const void * self, class_storeage_u object)
{
  const struct ClassContainer * const * cp = self;
  assert(self && *cp && (*cp)->count);
  return (*cp)->count(self, object);
}

// Find given element
void * coop_find_(void * self, class_storeage_u object)
{
  const struct ClassContainer * const * cp = self;
  assert(self && *cp && (*cp)->find);
  return (*cp)->find(self, object);
}
