/*
 * Copyright (c) 2020 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 */

#ifndef NEW_H_
#define NEW_H_

#include <stdio.h>	// for size_t

#include <stdint.h>
#include <stdarg.h>

#include "macro_num_args.h"

#define new(type, ... ) new_(type, NARGS(__VA_ARGS__), __VA_ARGS__)

/**
 * This function allocate memory for the object and copy the class data memory (see first argument) to this memory -> now we have an object.
 * Afterwards it calls the constructor with the given arguments for this object.
 * The delete function has to free the memory.
 * The constructor of the class has to free the object memory (for example the memory of text data).
 *
 * Note that only explicitly visible functions like new() can have a variable parameter list.
 * The list is accessed with a va_list variable ap which is initialized using the macro va_start() from stdarg.h.
 * new() can only pass the entire list to the constructor; therefore,
 * .ctor is declared with a va_list parameter and not with its own variable parameter list.
 * Since we might later want to share the original parameters
 *
 * @param class, pointer to class object memory (with size, function pointers to constructor, destructor, ...)
 * @param ..., variable parameters
 * @return returns the pointer of the object memory
 *
 * @see void delete(void * self)
 */
void * new_(const void * type, unsigned int numargs, ...);

/**
 * First call the destructor of the class.
 * It frees the object memory -> for example the text buffer of the string class
 * Afterwards free the memory of the class data structure.
 *
 * @param self, is the pointer to the object memory
 * @see *new(const void * _class, ...)
 */
void delete(void * self);

#endif /* NEW_H_ */
