/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 * 
 * This is the base class of all containers as CSet, CList, ....
 */

#ifndef C_CONTAINER_H_
#define C_CONTAINER_H_

#include "class.h"

/**
 * @brief Data structure of the class Container
 */
struct ClassContainer {
  const struct Class class;   /* Class methods must be first */

  /* Additional methods */
  int (*insert)(void * self, unsigned int numargs, va_list * app);
  int (*erase)(void * self, unsigned int pos);
  void * (*get)(const void * self, size_t pos);
  unsigned int (*count)(const void * self, class_storeage_u object);
  void * (*find)(void * self, class_storeage_u object);
};

/**
 * Insert new object
 * @param self, pointer to class data
 * @param object, pointer to object
 * @return error, 0 = no error, -1 = not unique
 */
#define coop_insert(self, ... ) coop_insert_(self, NARGS(__VA_ARGS__), __VA_ARGS__)
int coop_insert_(void * self, unsigned int numargs, ... );

/**
 * Erase element from container on given position
 * @param self, pointer to class data
 * @param pos, position
 * @return error code
 */
int coop_erase(void * self, unsigned int pos);

/**
 * Get the object on position pos.
 * @param self, pointer to class data
 * @param pos, position
 * @return void* pointer to object
 */
void * coop_get(const void * self, size_t pos);

/**
 * Count number of given element
 * @param self, pointer to class data
 * @param object, Count this elements
 * @return number of elements, 0 if not found
 */
#define coop_count(self, object) coop_count_(self, (class_storeage_u)object)
unsigned int coop_count_(const void * self, class_storeage_u object);

/**
 * Find given element
 * @param self, pointer to class data
 * @param object, pointer to element
 * @return Return the object in NULL if not found
 */
#define coop_find(self, object) coop_find_(self, (class_storeage_u)object)
void * coop_find_(void * self, class_storeage_u object);

#endif // C_CONTAINER_H_
