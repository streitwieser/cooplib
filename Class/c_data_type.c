/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 */

#include <assert.h>
#include "c_data_type.h"

// Append new elements to object
int coop_append_(void * self, unsigned int numargs, ... )
{
  const struct ClassDataType * const * cp = self;
  assert(self && *cp && (*cp)->append);

  va_list ap;
  va_start(ap, numargs);
  int error = (*cp)->append(self, numargs, &ap );
  va_end(ap);

  return error;
}
