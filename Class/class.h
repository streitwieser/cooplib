/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 *
 * Many objects will share the same type descriptor, i.e., they need the same amount of memory and the same methods can be applied to them.
 * We call all objects with the same type descriptor a class; a single object is called an instance of the class.
 * So far a class, an abstract data type, and a set of possible values together with operations, i.e., a data type, are pretty much the same.
 * An object is an instance of a class, i.e., it has a state represented by the memory allocated by new() and the state is manipulated with the methods of its class.
 * Conventionally speaking, an object is a value of a particular data type.
 */

#ifndef CLASS_H_
#define CLASS_H_

#include <stdio.h>	// for size_t
#include <stdarg.h> // va macros

#define NO_ERROR  0

/**
 * @brief Data structure of the class
 *
 *  Each of our objects starts with a pointer to its own type description, and through
 *  this type description we can locate type-specific information for the object:
 *  .size, is the length that new() allocates for the object;
 *  .ctor, points to the constructor called by new() which receives the allocated area and the rest of the argument list passed to new() originally;
 *  .dtor, points to the destructor called by delete() which receives the object to be destroyed
 *  .assign, assign new elements to object
 *  .clear, clears all elements from object
 *  .copy, it's a copy function which returns a new but identically object
 *  .compare, a function which compares the object to something else
 *  .to_string, returns the object data as a string
 *  .cout, points to a function that outputs the object data to a console.
 *
 * Each additional class contains this common definition, and thus these common functions.
 */
struct Class {
	size_t sizeOf;
	void * (*ctor)(void * self, unsigned int numargs, va_list * app);
	void * (*dtor)(void * self);
  size_t (*size)(const void * self);
  int (*assign)(void * self, unsigned int numargs, va_list * app);
	void (*clear)(void * self);
	void * (*copy)(const void * self);
  int (*compare)(const void * self, const void * b);
  char * (*to_string)(const void * self);
	void(*cout)(const void * self);
};

// Valid data types of all classes
typedef enum { 
  COOP_UNKOWN = 0,  //!< not initializes or cleared
  COOP_CHAR,        //!< object elements are chars
  COOP_INT,         //!< object elements are ints
  COOP_LONG,        //!< object elements are longs
  COOP_FLOAT,       //!< object elements are floats
  COOP_DOUBLE,      //!< object elements are doubles
  COOP_OBJECT,      //!< object elements are objects
} class_type_e;

// Storage for data type of a class
typedef union {
  char c;
  int i;
  long l;
  float f;
  double d;
  void * v;
} class_storeage_u;

/**
 * Gets the size of the object memory
 * @param self, is the pointer to the object memory
 * @return size of object memory
 */
size_t coop_sizeOf(const void * self);

/**
 * Returns the number of elements of the object
 * @param self, pointer to class data
 * @return Number of elements
 */
size_t coop_size(const void * self);


/**
 * Assign new elements to object
 * @param self, pointer to class data
 * @param numargs, number of variable arguments
 * @param args, an argument list with elements
 * @return error, 0 = no error
 */
#define coop_assign(self, ... ) coop_assign_(self, NARGS(__VA_ARGS__), __VA_ARGS__)
int coop_assign_(void * self, unsigned int numargs, ... );

/**
 * Clear elements from object
 * @param self, pointer to class data
 */
void coop_clear(void * self);

/**
 * Copy object
 * @param self, is the pointer to the object memory
 * @return pointer to memory of new object
 */
void * coop_copy( const void * self);

/**
 * Compares two objects
 * @param self, pointer to class data
 * @param second, pointer to second object
 * @return: 0: is both objects are equal
 *         <0: self is lower than second
 *         >0: self if higher than second
 */
int coop_compare(const void * self, const void * second);

/**
 * Converts the object data to a string.
 * @param self, is the pointer to the object memory
 * @return Pointer to char array
 */
char * coop_toString(const void * self);

/**
 * Outputs the object data to the console.
 * @param self, is the pointer to the object memory
 */
void coop_cout(const void * self);

// Helper functions 

int coop_compare_long(const void *p1, const void *p2);
int coop_compare_int(const void *p1, const void *p2);
int coop_compare_double(const void *p1, const void *p2);
int coop_compare_float(const void *p1, const void *p2);

#endif // CLASS_H_
