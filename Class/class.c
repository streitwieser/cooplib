/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 */

#include <assert.h>

#include <math.h>     // for fabs()
#include <float.h>    // __DBL_EPSILON__

#include "class.h"

// Gets the size of the object memory
size_t coop_sizeOf(const void * self)
{
  const struct Class * const * cp = self;
  assert(self && *cp);
  return (*cp)->sizeOf;
}

// Returns the number of elements of the object
size_t coop_size(const void * self)
{
  const struct Class * const * cp = self;
  assert(self && *cp && (*cp)->size);
  return (*cp)->size(self);
}

// Assign new elements to object
int coop_assign_(void * self, unsigned int numargs, ... )
{
  const struct Class * const * cp = self;
  assert(self && *cp && (*cp)->assign);

  va_list ap;
  va_start(ap, numargs);
  int error = (*cp)->assign(self, numargs, &ap );
  va_end(ap);

  return error;
}

// Clear elements from object
void coop_clear(void * self)
{
  const struct Class * const * cp = self;
  assert(self && *cp && (*cp)->clear);
  return (*cp)->clear(self);
}

// Copy object
void * coop_copy(const void * self)
{
  const struct Class * const * cp = self;
  assert(self && *cp && (*cp)->copy);
  return (*cp)->copy(self);
}

// Compares two objects
int coop_compare(const void * self, const void * second)
{
  const struct Class * const * cp = self;
  assert(self && *cp && (*cp)->compare);
  return (*cp)->compare(self, second);
}

// Converts the object data to a string.
char * coop_toString(const void * self)
{
  const struct Class * const * cp = self;
  assert(self && *cp && (*cp)->to_string);
  return (*cp)->to_string(self);
}

// Outputs the object data to the console.
void coop_cout(const void *self)
{
  const struct Class * const * cp = self;
  assert(self && *cp && (*cp)->cout);

  if (self && *cp && (*cp)->cout)
    (*cp)->cout(self);
}

//###########################################################################################
// Helper functions
// Functions that compares elements
//###########################################################################################

int coop_compare_long(const void *p1, const void *p2)
{
  const unsigned long *pn1 = p1;
  const unsigned long *pn2 = p2;

  if( *pn1 == *pn2 )
    return 0;
  else if( *pn1 < *pn2 )
    return -1;
   else
     return 1;
}

int coop_compare_int(const void *p1, const void *p2)
{
  const unsigned int *pn1 = p1;
  const unsigned int *pn2 = p2;

  return coop_compare_long(pn1, pn2);
}

int coop_compare_double(const void *p1, const void *p2)
{
  const double *pn1 = p1;
  const double *pn2 = p2;

  if( fabs(*pn1-*pn2) < __DBL_EPSILON__ )
    return 0;
  else if( *pn1 < *pn2 )
    return -1;
   else
     return 1;
}

int coop_compare_float(const void *p1, const void *p2)
{
  const float *pn1 = p1;
  const float *pn2 = p2;

  if (fabs(*pn1 - *pn2) < __DBL_EPSILON__)
    return 0;
  else if (*pn1 < *pn2)
    return -1;
  else
    return 1;
}
