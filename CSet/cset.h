/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_SET_H_
#define C_SET_H_

#include "../Class/c_container.h"

enum { CSET_ERROR_ID = 0x0000000043536574 }; // CSet in hex

/**
 * @brief Define error type for container CSet 
 */
typedef enum
{
  CSET_NO_ERROR = NO_ERROR,
  CSET_INVALID_OBJECT = ((CSET_ERROR_ID & 0x00FFFFFF) << 8 | 0x80000001),
  CSET_NOT_UNIQUE = ((CSET_ERROR_ID & 0x00FFFFFF) << 8 | 0x80000002),
  CSET_INVALID_POSITION = ((CSET_ERROR_ID & 0x00FFFFFF) << 8 | 0x80000003),
} cset_error_t;

/**
 * Define node structure for list of objects
 */
typedef struct cset_node
{
  class_storeage_u object;  //!< store object
  struct cset_node *next;   //!< pointer to next node
} cset_node_t;

/**
 * The set class.
 * It contains the class. With sizeOf, constructor, destructor, and other methods.
 * And is contains additional data fields of CSet.
 */
struct CSet
{
  const void *ClassContainer; /* must be first */

  class_type_e type;                          /* store type of set */
  int (*compare)(void const *, void const *); /* compare mehtod of object */
  cset_node_t *head;                          /* linked list */
  size_t elements;                            /* number of elements */
};

/** Pointer to the set class memory */
extern const void *CSet;

// Wrapper for Class

/** Gets the size of the object memory. It is a wrapper method for coop_sizeOf() */
extern size_t cset_sizeOf(const void *self);
/** Returns the number of objects stored into set. It is a wrapper macro for coop_size()  */
extern size_t cset_size(const void *self);
/** Assign new elements to object. It is a wrapper macro for coop_assign() */
#define cset_assign(self, ...) coop_assign_(self, NARGS(__VA_ARGS__), __VA_ARGS__)
/** Clear all elements of CSet. It is a wrapper method for coop_clear() */
extern void cset_clear(void *self);
/** Copy CSet object. It is a wrapper method for coop_copy() */
extern void *cset_copy(const void *self);
/** Compares two CSet objects. It is a wrapper method for coop_compare() */
extern int cset_compare(const void *self, const void *second);
/** Converts the CSet to a string. It is a wrapper method for coop_toString() */
extern char *cset_toString(const void *self);
/** Outputs CSet as text to the console. It is a wrapper method for coop_cout() */
extern void cset_cout(const void *self);

// Wrapper for ClassContainer

/** This macro casts the argument of cset_insert */
#define CSET_INSERT_ARGS(object) (class_storeage_u)object

/** Insert new object. It is a wrapper macro for coop_insert() */
#define cset_insert(self, ...) coop_insert_(self, NARGS(__VA_ARGS__), CSET_INSERT_ARGS(__VA_ARGS__) )
/** Erase element from set on given position. It is a wrapper method for coop_erase() */
int cset_erase(void *self, unsigned int pos);
/** Get element on position pos. It is a wrapper method for coop_get() */
void *cset_get(const void *self, size_t pos);
/** Count number of given element. It is a wrapper macro for coop_count() */
#define cset_count(self, object) coop_count_(self, (class_storeage_u)object)
/** Find given element. It is a wrapper macro for coop_find() */
#define cset_find(self, object) coop_find_(self, (class_storeage_u)object)

// Methods only for CSet

#endif /* C_SET_H */