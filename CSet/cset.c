/*
 * Copyright (c) 2021 Johann Streitwieser.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * *****************************************************************************************
 * Thanks to Axel-Tobias Schreiner for his great book "Object-Orient Programming With ANSI-C"
 * Please see and read https://www.freetechbooks.com/axel-tobias-schreiner-a3189.html
 * *****************************************************************************************
 */

#include <assert.h>

#include <string.h> // strlen(), strcpy()
#include <stdlib.h> // malloc()

#include "../Class/new.h"

#include "cset.h"

// pre declaration of all methods

// Class methods
static void *Set_ctor(void *self, unsigned int numargs, va_list *app);
static void *Set_dtor(void *self);
static size_t Set_size(const void *self);
static int Set_assign(void *self, unsigned int numargs, va_list *app);
static void *Set_copy(const void *self);
static char *Set_to_string(const void *self);
static void Set_cout(const void *self);

// Container methods
static int Set_insert(void *self, unsigned int numargs, va_list *app);
static void Set_clear(void *self);
static int Set_erase(void *self, unsigned int pos);
static unsigned int Set_count(const void *self, class_storeage_u object);
static void *Set_find(void *self, class_storeage_u object);
static void *Set_get(const void *self, size_t pos);
static int Set_compare(const void *self, const void *b);

struct ClassSet
{
  const struct ClassContainer class;  /* Base class methods must be first */
};

/*
 * Define new type CSet class
 * SizeOf is the size of this data structure, size of the class Set.
 * The next function pointers assign the construct, destructor, ...
 */
static const struct ClassSet CSetClass = {
    /* Attributes and methods of Class */
    {
        .class.sizeOf = sizeof(struct CSet),

        .class.ctor = Set_ctor,
        .class.dtor = Set_dtor,
        .class.size = Set_size,
        .class.assign = Set_assign,
        .class.clear = Set_clear,
        .class.copy = Set_copy,
        .class.compare = Set_compare,
        .class.to_string = Set_to_string,
        .class.cout = Set_cout,

        .insert = Set_insert,
        .erase = Set_erase,
        .get = Set_get,
        .count = Set_count,
        .find = Set_find,
    },

    /* Methods of class CSet */
};

/** This is the memory of the CLASS data (not of the object data) */
const void *CSet = &CSetClass;

// Constructor of the Set class
//
// The constructor retrieves the text passed to new() and stores a dynamic copy in the struct Set which was allocated by new().
// In the constructor we only need to initialize object data because new() has already set up .class.
// app: arguments, here the data type of the set and a method to compare inserted data
static void *Set_ctor(void *self, unsigned int numargs, va_list *app)
{
  struct CSet *cset = self;
  assert(numargs == 2);

  cset->type = va_arg(*app, class_type_e);                           // type of set
  cset->compare = va_arg(*app, int (*)(void const *, void const *)); // compare method of object

  cset->head = NULL;
  cset->elements = 0;

  return cset;
}

// Destructor of the Set class.
// The destructor frees the dynamic memory controlled by the Set.
// Since delete() can only call the destructor if self is not null, we do not need to check things
static void *Set_dtor(void *self)
{
  struct CSet *cset = self;

  Set_clear(cset);

  cset->head = NULL;
  cset->elements = 0;

  return cset;
}

// Returns the number of objects stored into set
static size_t Set_size(const void *self)
{
  const struct CSet *csp = self;
  return csp->elements;
}

// Assign new objects to set
static int Set_assign(void *self, unsigned int numargs, va_list *app)
{
  cset_error_t error = NO_ERROR;
  class_storeage_u object = {0};

  Set_clear(self);

  for (unsigned int i = 0; i < numargs; i++)
  {
    object = va_arg(*app, class_storeage_u);
    error |= cset_insert(self, object);
  }

  return error;
}

// Clear set
static void Set_clear(void *self)
{
  struct CSet *cset = self;

  cset_node_t *p = cset->head;
  void *pfree = NULL;

  while (p != NULL)
  {

    if (cset->type == COOP_OBJECT)
    {
      delete (p->object.v);
    }

    pfree = p;
    p = (cset_node_t *)p->next;

    free(pfree);
  }

  cset->head = NULL;
  cset->elements = 0;
}

// Copy set object.
// Set_copy() makes a copy of a Set. Later both, the original and the copy,
// will be passed to delete() so we must make a new dynamic copy of the object
// This can easily be done by calling new() and insert().
static void *Set_copy(const void *self)
{
  struct CSet const *cset = self;
  cset_node_t *p = cset->head;

  void *new_set = new (CSet, cset->type, cset->compare);
  assert(new_set != NULL);

  while (p != NULL)
  {
    cset_insert(new_set, p->object);
    p = (cset_node_t *)p->next;
  }

  return new_set;
}

// Compares the both CSet objects
static int Set_compare(const void *self, const void *b)
{
  struct CSet const *cset = self;
  struct CSet const *check = b;

  cset_node_t *first = cset->head;
  cset_node_t *second = check->head;

  int cmp = 0;

  while (first != NULL && second != NULL)
  {
    if (cset->type == COOP_OBJECT)
      cmp = cset->compare(first->object.v, second->object.v);
    else
      cmp = cset->compare(&first->object, &second->object);

    if (cmp != 0)
      break;

    first = first->next;
    second = second->next;
  }

  if (first != NULL && second == NULL)
    return 1;

  if (first == NULL && second != NULL)
    return -1;

  return cmp;
}

// Get object as char[]
static char *Set_to_string(const void *self)
{
  char buffer[100];
  struct CSet const *cset = self;
  cset_node_t *p = cset->head;
  size_t size = 0, len = 0;
  char *pChar = NULL;
  char *pText = NULL;

  if ( p != NULL)
  {
    while (p != NULL)
    {
      if (cset->type == COOP_OBJECT)
      {
        pChar = cset_toString(p->object.v);
        len = strlen(pChar) + 1;
      }
      else
      {
        switch (cset->type)
        {
        case COOP_CHAR:   sprintf(buffer, "%c", p->object.c); break;
        case COOP_INT:    sprintf(buffer, "%d", p->object.i); break;
        case COOP_LONG:   sprintf(buffer, "%ld", p->object.l);  break;
        case COOP_FLOAT:  sprintf(buffer, "%.3f", p->object.f); break;
        case COOP_DOUBLE: sprintf(buffer, "%.6f", p->object.d); break;
        case COOP_OBJECT:
        default:
          assert(0);
          break;
        }

        pChar = buffer;
        len = strlen(pChar) + 1;
      }

      if (size == 0)
      {
        len++;
        pText = calloc(1, len);
        size += len;
        pText = strcat(pText, "(");
      }
      else
      {
        size += len + 1;
        pText = realloc(pText, size);
        pText = strcat(pText, ",");
      }

      pText = strcat(pText, pChar);

      if (cset->type == COOP_OBJECT)
        free(pChar);

      p = (cset_node_t *)p->next;
    }

    pText = realloc(pText, size + 2);
    pText = strcat(pText, ")");
  }
  else
  {
    pText = calloc(1, 3);
    pText = strcat(pText, "()");
  }

  return pText;
}

// Output text to console
static void Set_cout(const void *self)
{
  char *p = Set_to_string(self);
  printf("%s\n", p);
  free(p);
}

// Check if object is unique
// Try to get to end of list, if so then object not found.
// If object is found break and return with NOT_UNIQUE
static int check_for_identical_object(void *self, cset_node_t **p, class_storeage_u *object)
{
  struct CSet *cset = self;

  while (*p != NULL)
  {
    if (cset->type == COOP_OBJECT)
    {
      class_storeage_u store;
      memcpy(&store, &(*p)->object, sizeof(class_storeage_u));
      if (cset->compare(object->v, store.v) == 0)
        return CSET_NOT_UNIQUE;
    }
    else
    {
      if (cset->compare(object, &(*p)->object) == 0)
        return CSET_NOT_UNIQUE;
    }
    p = &(*p)->next;
  }

  return NO_ERROR;
}

// Insert new object to set
// Use intersort for new object
static int Set_insert(void *self, unsigned int numargs, va_list *app)
{
  struct CSet *cset = self;
  class_storeage_u object = {0};
  class_storeage_u store = {0};
  assert( numargs == 1);

  object = va_arg(*app, class_storeage_u);
  if (cset->type == COOP_OBJECT)
  {
    void *new = cset_copy(object.v);
    assert(new != NULL);
    store.v = new;
  }
  else
  {
    memcpy(&store, &object, sizeof(class_storeage_u));
  }

  cset_error_t error = check_for_identical_object(self, &cset->head, &store);
  if (!error)
  {
    cset_node_t *p = (cset_node_t *)calloc(1, sizeof(cset_node_t));
    memcpy(&p->object, &store, sizeof(class_storeage_u));

    if (cset->head == NULL)
    {
      // first element
      p->next = cset->head;
      cset->head = p;
    }
    else
    {
      cset_node_t *tmp = cset->head;
      cset_node_t *prev = NULL;

      while (tmp != NULL)
      {
        if (cset->type == COOP_OBJECT)
        {
          if (cset->compare(object.v, tmp->object.v) < 0)
            break;
        }
        else
        {
          if (cset->compare(&object, &tmp->object) < 0)
            break;
        }

        prev = tmp;
        tmp = tmp->next;
      }

      p->next = tmp;
      if (prev != NULL)
        prev->next = p;
      else
        cset->head = p;
    }

    cset->elements++;
  }
  return error;
}

// Erase element from set on given position
static int Set_erase(void *self, unsigned int pos)
{
  struct CSet *cset = self;
  class_storeage_u object = {0};
  cset_node_t *p = cset->head;
  void *pfree = NULL;

  if (pos > cset->elements - 1)
    return CSET_INVALID_POSITION;

  // search element
  if (pos == 0)
  {
    memcpy(&object, &cset->head->object, sizeof(class_storeage_u));
    pfree = cset->head;
    cset->head = cset->head->next;
  }
  else
  {
    pos--;
    while (p != NULL && pos > 0)
    {
      p = p->next;
      pos--;
    }

    // free this object back
    object = p->next->object;
    pfree = p->next;

    // current pointer points now to next element!
    p->next = p->next->next;
  }

  if (cset->type == COOP_OBJECT)
    delete (object.v);

  free(pfree);

  cset->elements--;
  return NO_ERROR;
}

// Get the object on position pos.
// Construct new object that is equal to object on pos!
static void *Set_get(const void *self, size_t pos)
{
  struct CSet const *cset = self;
  cset_node_t *p = cset->head;

  while (p != NULL && pos > 0)
  {
    pos--;
    p = p->next;
  }

  if (p != NULL)
  {
    if (cset->type == COOP_OBJECT)
    {
      class_storeage_u store = p->object;
      void *p = cset_copy(store.v);
      return p;
    }
    else
    {
      // we return a void pointer to memory -> access memory.
      void *o = calloc(1, sizeof(class_storeage_u));
      memcpy(o, &p->object, sizeof(class_storeage_u));
      return o;
    }
  }

  return NULL;
}

// Count number of object into set
static unsigned int Set_count(const void *self, class_storeage_u object)
{
  struct CSet const *cset = self;
  cset_node_t const *p = cset->head;
  unsigned int count = 0, equal = 0;

  while (p != NULL)
  {
    if (cset->type == COOP_OBJECT)
      equal = cset->compare(p->object.v, object.v) == 0;
    else
      equal = cset->compare(&p->object, &object) == 0;

    if (equal)
      count++;

    p = p->next;
  }

  return count;
}

// Find given object into set
static void *Set_find(void *self, class_storeage_u object)
{
  struct CSet *cset = self;
  unsigned int equal = 0;
  cset_node_t *p = cset->head;

  while (p != NULL)
  {
    if (cset->type == COOP_OBJECT)
      equal = cset->compare(p->object.v, object.v) == 0;
    else
      equal = cset->compare(&p->object, &object) == 0;

    if (equal)
      break;

    p = p->next;
  }

  if (equal)
  {
    if (cset->type == COOP_OBJECT)
      return p->object.v;
    else
      return &p->object;
  }
  else
    return NULL;
}

//###########################################################################################
// Wrapper metods for Class and ClassContainer
//###########################################################################################

// Wrapper for Class
inline size_t cset_sizeOf(const void *self) { return coop_sizeOf(self); }
inline size_t cset_size(const void *self) { return coop_size(self); }
inline void cset_clear(void *self) { coop_clear(self); }
inline void *cset_copy(const void *self) { return coop_copy(self); }
inline int cset_compare(const void *self, const void *second) { return coop_compare(self, second); }
inline char *cset_toString(const void *self) { return coop_toString(self); }
inline void cset_cout(const void *self) { coop_cout(self); }

// Wrapper for ClassDataType

int cset_erase(void *self, unsigned int pos) { return coop_erase(self, pos); }
void *cset_get(const void *self, size_t pos) { return coop_get(self, pos); }

//###########################################################################################
// Access class methods over this functions -> check pointers, ...
//###########################################################################################
